"""
Extracts AMS across the whole MN grid from each scenario's set of 20 single-year netcdfs.
"""

import os
import numpy as np
import pandas as pd
import netCDF4 as nc
import datetime as dt
from pathlib import Path
import xarray as xr
import rioxarray as rio
from scipy import interpolate, stats
from lmoments3a import distr
from AMS_to_DDF import GEVfit

scenario_dict = {'HIST': 'HISTORIC/1980-1999', 'MID': 'RCP4.5/2040-2059', 'END4.5': 'RCP4.5/2080-2099',
                 'END8.5': 'RCP8.5/2080-2099'}

year_dict = {'HIST': [1980, 1999], 'MID': [2040, 2059], 'END4.5': [2080, 2099], 'END8.5': [2080, 2099]}

model_dict = {'BC': 'bcc-csm1-1', 'CC': 'CCSM4', 'CM': 'CMCC-CM', 'CN': 'CNRM-CM5', 'GF': 'GFDL-ESM2M',
              'IP': 'IPSL-CM5A-LR', 'MR': 'MRI-CGCM3', 'MI': 'MIROC5'}

inputs_path = 'C:/Users/rrnoe/Work/GIS/MN/climate/LCCMR_IBIS'
AMSgrid_path = 'C:/Users/rrnoe/Work/Stormwater/v3/modeled_AMS_grids'
DDFgrid_path = 'C:/Users/rrnoe/Work/Stormwater/v3/modeled_DDF_grids'


templatepath = os.path.join(inputs_path, 'bcc-csm1-1', 'HISTORIC/1980-1999', 'WRF_IBISinput/IBISinput_1980.nc')
templategrid = xr.open_dataset(templatepath)

# da = rio.open_rasterio('universal_mask2.tif').squeeze()
# da = da.rename({"y": "lat", "x": "lon"})
# templategrid.coords['mask'] = (('lat', 'lon'), da.data)

class ModelGrid:

    def __init__(self, in_path, model, scenario, out_path):
        self.in_path = in_path
        self.model = model
        self.scenario = scenario
        self.out_path = out_path
        self.yrInit, self.yrEnd = year_dict[scenario][0:2]
        # self.lats, self.lons = self.getLatLon()
        self.dur_list = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

    def Input(self, year):  # can input year=0 through 19 (yrEnd - yrInit)
        Year = self.yrInit + year
        ncfile = os.path.join(self.in_path,
                              model_dict[self.model],
                              scenario_dict[self.scenario],
                              'WRF_IBISinput/IBISinput_{}.nc'.format(Year))
        yeargrid = xr.open_dataset(ncfile)
        yeargrid = yeargrid.drop(['lev', 'tmin', 'tmax', 'relh', 'wspd', 'rads', 'snow'], errors='ignore')
        yeargrid = xr.where(yeargrid > 1e+3, np.nan, yeargrid)

        yeargrid = yeargrid.assign_coords(lat=templategrid.lat)
        yeargrid = yeargrid.assign_coords(lon=templategrid.lon)

        # Set the model calibration zone (5 pixel border) to nan
        yeargrid[dict(lat=slice(0,100), lon=slice(0, 5))] = np.nan
        yeargrid[dict(lat=slice(0,100), lon=slice(125, 130))] = np.nan
        yeargrid[dict(lat=slice(0,5), lon=slice(0, 130))] = np.nan
        yeargrid[dict(lat=slice(95,100), lon=slice(0, 130))] = np.nan

        return yeargrid

    def AnnualMax(self, year, duration):
        yeargrid = self.Input(year).squeeze()  # squeeze removes extraneous 'lev' dimension
        cumsumgrid = yeargrid.rolling(time=duration, center=False).sum()
        maxgrid = cumsumgrid.max(dim='time')
        return maxgrid.rename_vars({'prec': 'precmax'})

    def CombineYears(self, duration):
        annualmax_list = [self.AnnualMax(a, duration) for a in range(20)]
        AMSgrid = xr.concat(annualmax_list, pd.Index(range(self.yrInit, self.yrEnd + 1), name='year'))
        return AMSgrid

    def CombineDurations(self):
        AMSbydur_list = [self.CombineYears(a) for a in self.dur_list]
        AMSgrid = xr.concat(AMSbydur_list, pd.Index(self.dur_list, name='duration'))
        return AMSgrid

    def createNC(self):
        outpath = os.path.join(self.out_path, self.scenario, self.model)
        Path(outpath).mkdir(parents=True, exist_ok=True)
        outfile = os.path.join(outpath, '20year_AMS.nc')

        AMSgrid = self.CombineDurations()
        AMSgrid.to_netcdf(outfile, 'w', format='NETCDF3_CLASSIC')


def createNC_fullset() -> object:
    for scenario in scenario_list:
        for model in model_dict.keys():
            ModelGrid(inputs_path, model, scenario, AMSgrid_path).createNC()


def ensemble_fromAMS():
    for scenario in scenario_list:
        AMSbymodel_list = [ModelGrid(inputs_path, M, scenario, AMSgrid_path).CombineDurations()
                           for M in model_dict.keys()]
        AMSgrid = xr.concat(AMSbymodel_list, pd.Index(model_dict.keys(), name='model'))
        AMSgrid = AMSgrid.mean(dim='model')

        outpath = os.path.join(AMSgrid_path, scenario, 'ensemble_fromAMS')
        Path(outpath).mkdir(parents=True, exist_ok=True)
        outfile = os.path.join(outpath, '20year_AMS.nc')
        AMSgrid = AMSgrid.rio.write_crs('EPSG:4326')
        AMSgrid.to_netcdf(outfile, 'w', format='NETCDF3_CLASSIC')

        # AMSgrid = AMSgrid.drop(labels=['mask', 'msk'])
        AMSgrid = AMSgrid.rename({'lon': 'x', 'lat': 'y'})
        # AMSgrid.sel(duration=1, year=1995).rio.to_raster("AMS_20yr.tif")


# fit curve to each gridcell's individual AMS, after netcdfs have been created (above)         
class GridStats:

    def __init__(self, in_path, scenario, out_path):
        self.in_path = in_path
        # self.model = model  # only working with AMS-ensemble for now
        self.scenario = scenario
        self.out_path = out_path
        self.dur_list = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
        # self.dur_list = [1, 2]

    def Input(self):
        # if using ensemble data:
        # ncfile = os.path.join(self.in_path, self.scenario, 'ensemble_fromAMS/20year_AMS.nc')
        # AMSgrid = xr.open_dataset(ncfile)
        # return AMSgrid

        ncfile = os.path.join(self.in_path, self.scenario, 'BC', '20year_AMS.nc')
        AMSgrid_base = xr.open_dataset(ncfile)
        AMSgrid_base = AMSgrid_base.where(AMSgrid_base!=AMSgrid_base.year[:-1*(drop_last_n)])
        model_list = ['CC', 'CM', 'CN', 'GF', 'MI', 'MR', 'IP']

        for model in model_list:
            ncfile = os.path.join(self.in_path, self.scenario, model, '20year_AMS.nc')
            AMSgrid = xr.open_dataset(ncfile)
            AMSgrid = AMSgrid.where(AMSgrid != AMSgrid.year[:-1*(drop_last_n)])
            AMSgrid_base = xr.concat([AMSgrid_base, AMSgrid], 'year')

        return AMSgrid_base

    def GEVfunc(self, AMSarray, dur):
        gev_fit = GEVfit(AMSarray, dur, True)
        return gev_fit.pts_on_curve()[0:6]  # skips longer recurrence intervals >100y

    def applyGEV(self, dur):
        AMS = self.Input().sel(duration=dur)

        DDF = xr.Dataset(
            {'prec': (['lat', 'lon', 'recurrence'],
                      np.zeros((100, 130, 6)))},
            coords={'lat': AMS.lat, 'lon': AMS.lon,
                    'recurrence': [2, 5, 10, 25, 50, 100]}
        )

        for latNo in range(len(AMS.lat)):
            for lonNo in range(len(AMS.lon)):
                AMS_array = AMS.isel(lat=latNo, lon=lonNo).to_array()
                arrsum = AMS_array.values.sum()
                if np.isnan(arrsum):
                    DDF['prec'][latNo, lonNo, :] = np.nan
                else:
                    ddf = self.GEVfunc(AMS_array.values.flatten(), dur)
                    DDF['prec'][latNo, lonNo, :] = ddf

        # later: could see if apply_ufunc (or similar) can acheive this more efficiently than through "for" loops
        # possible incompatilibity between lmoments3 and xarray?
        # this didn't work but something vaguely similar might??
        # newgrid = xr.apply_ufunc(self.GEVfunc, AMS, input_core_dims=['year'], kwargs={'dur':dur})
        # newgrid = xr.apply_ufunc(self.GEVfunc, AMS, input_core_dims=[['year']], kwargs={'dur':dur})

        return DDF

    def CombineDurationsOld(self):
        DDFbydur_list = [self.applyGEV(a) for a in self.dur_list]
        DDFgrid = xr.concat(DDFbydur_list, pd.Index(self.dur_list, name='duration'))
        return DDFgrid

    def CombineDurations(self):
        DDFbydur_list = [self.applyGEV(a) for a in self.dur_list]
        DDF = xr.concat(DDFbydur_list, pd.Index(self.dur_list, name='duration'))

        dur_list = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

        DDFsmoothed = xr.Dataset(
            {'precspline': (['lat', 'lon', 'recurrence', 'duration'],
                            np.zeros((100, 130, 6, 10)))},
            coords={'lat': DDF.lat, 'lon': DDF.lon,
                    'recurrence': [2, 5, 10, 25, 50, 100],
                    'duration': dur_list}
        )
        for AEP in range(len(DDF.recurrence)):
            for latNo in range(len(DDF.lat)):
                for lonNo in range(len(DDF.lon)):
                    DDF_array = DDF.isel(lat=latNo, lon=lonNo, recurrence=AEP).to_array()
                    arrsum = DDF_array.values.sum()
                    if np.isnan(arrsum):
                        DDFsmoothed['precspline'][latNo, lonNo, AEP, :] = np.nan
                    else:
                        spline_func = interpolate.UnivariateSpline(dur_list[3:], DDF_array.values.flatten()[3:])
                        ddf = spline_func(dur_list)
                        DDFsmoothed['precspline'][latNo, lonNo, AEP, :] = ddf
        return DDFsmoothed

    def createNC(self):
        outpath = os.path.join(self.out_path)
        Path(outpath).mkdir(parents=True, exist_ok=True)

        filename = '{}_DDF.nc'.format(self.scenario)
        outfile = os.path.join(self.out_path, filename)

        AMSgrid = self.CombineDurations()
        AMSgrid.rio.write_crs(4326, inplace=True)
        AMSgrid.to_netcdf(outfile, 'w', format='NETCDF3_CLASSIC')
        AMSgrid = AMSgrid.rename({'lon': 'x', 'lat': 'y'})
        AMSgrid = AMSgrid / 25.4

        for d in [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]:
            for yr in [2, 5, 10, 25, 50, 100]:
                outfile = os.path.join(self.out_path, f"{self.scenario}_DDF_{d}d_{yr}yr.tif")
                ras = AMSgrid.sel(duration=d, recurrence=yr).to_array()
                ras.rio.write_nodata(-9999, inplace=True)
                ras.rio.to_raster(outfile)


def ensemble_DDF(AMSpath=AMSgrid_path, DDFpath=DDFgrid_path):
    for scenario in scenario_list:
        outpath = os.path.join(DDFpath, scenario)
        Path(outpath).mkdir(parents=True, exist_ok=True)
        GridStats(AMSpath, scenario, outpath).createNC()


drop_last_n = 3
scenario_list = ['HIST', 'END4.5', 'END8.5']
# createNC_fullset()
ensemble_DDF()
