'''
This code calculates depth-duration frequencies from annual maximum series.
L-moment calculations from each duration (1d to 60d) are aggregrated for a 
single location/scenario/model.

If using historical observation AMS, can calculate from either the full period
of record or a designated subset of it.
'''

import numpy as np
import pandas as pd
from lmoments3a import distr
from scipy import interpolate
import os

# provides period of record for each location (most have years missing)
# end values are inclusive, as used in df.loc function
period_dict = {'ALX': [1889, 2010], 'DLH': [1941, 2012], 'MSP': [1837, 2010], 'RST': [1893, 2010], 'WRN': [1903, 1975]}

loc_dict = {'ALX': 'Alexandria', 'DLH': 'Duluth', 'MSP': 'Minneapolis', 'RST': 'Rochester', 'WRN': 'Warren'}

# Average recurrence intervals
ARI_list = np.array([2, 5, 10, 25, 50, 100, 200, 500, 1000])

# Durations used in Atlas14, in days (1d and above)
dur_list = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
durlabels = [str(a) + 'd' for a in dur_list]

scenario_dict = {'HIST', 'MID', 'END4.5', 'END8.5'}
model_dict = {'BC': 'bcc-csm1-1', 'CC': 'CCSM4', 'CM': 'CMCC-CM',
              'CN': 'CNRM-CM5', 'GF': 'GFDL-ESM2M', 'IP': 'IPSL-CM5A-LR',
              'MR': 'MRI-CGCM3', 'MI': 'MIROC5'}


# Annual exceedance probability (subtracted from 1 for PPF)
# def AEP(ARI):
#     aep = 1 / ARI
#     return 1 - aep

# returns constrained AMS for downloaded Atlas 14 inputs
# their precip values are already in inches
def AMS_obsv(dirc, loc='MSP', duration=1, fullrecord=True, init=1837, end=2010):
    # AMSfile = '/Users/birke111/Documents/Atlas14/inputs/{}_AMS.csv'.format(loc)
    AMSfile = os.path.join(dirc, '{}_AMS.csv'.format(loc))

    AMSdf = pd.read_csv(AMSfile, index_col=0)

    if fullrecord:
        init = period_dict[loc][0]
        end = period_dict[loc][1]

    # Include this to display ALL years without data if desired.
    # Could remove "else" if that's useful to know even when setting fullrecord=True.
    # However, printed message is repeatedly displayed if looping from sample_run.
    '''
    else: 
        nodata_years = [year for year in range(init,end) if year not in AMSdf.index]
        print('No data for years of selected record in {}:'.format(loc), nodata_years)
    '''

    AMSconstrained = AMSdf.loc[init:end, '{}d'.format(duration)].values
    AMSconstrained = AMSconstrained[~np.isnan(AMSconstrained)]

    return AMSconstrained


# returns constrained AMS for model output-based AMS files
# model='all' to average the 8 models' AMS together, otherwise select 1 model
# converts model precip values from mm to inches
def AMS_WRF(dirc, Loc='MSP', duration=1, scenario='HIST', model='', n_row_drop=0):
    if duration == 1:
        AMSfile = os.path.join(dirc, '{}_year_max_prec_ams.csv'.format(scenario))
    else:
        AMSfile = os.path.join(dirc, '{0}_year_max_consum_prec_{1}_days_ams.csv'.format(scenario, duration))
    AMSdf = pd.read_csv(AMSfile, index_col=0)

    if model == 'all':
        indivModelDF = AMSdf.groupby('year').agg('mean')
    else:
        indivModelDF = AMSdf[AMSdf.model == model_dict[model]]

        # old version - using 3letter code for the 5 municipalities. Now just using whole county names
    # AMSmm = indivModelDF.loc[:, loc_dict[Loc]].values
    indivModelDF.drop(indivModelDF.tail(n_row_drop).index, inplace=True)

    AMSmm = indivModelDF.loc[:, Loc].values
    AMSinches = AMSmm / 25.4
    return AMSinches


# string together AMS_WRF for 2 scenarios, create a 40-year "record"
def MultiScenario(dirc, loc='MSP', duration=1, scenario1='HIST', scenario2='MID', model='', n_row_drop=0):
    AMS1 = AMS_WRF(dirc, loc, duration, scenario1, model, n_row_drop=n_row_drop)
    AMS2 = AMS_WRF(dirc, loc, duration, scenario2, model, n_row_drop=n_row_drop)

    return np.append(AMS1, AMS2)


# AMS of any length as input (ndarray format)
# intended to be applicable to AMS from any source (should work for obsv too)
class GEVfit:

    def __init__(self, AMSarray, duration, applyCorrFactor=True):
        self.AMS = AMSarray

        CFdict = {1: 1.12, 2: 1.04, 3: 1.03, 4: 1.02, 7: 1.01, '>7': 1}
        Dur = duration
        if Dur > 7:
            Dur = '>7'
        corr_factor = CFdict[Dur]

        if applyCorrFactor:
            self.AMS = np.multiply(self.AMS, corr_factor)

    def AEP_old(self, ARI):
        aep = 1 / ARI
        return 1 - aep

    def AEP(self, ARI):  # https://tonyladson.wordpress.com/2017/07/04/converting-between-ey-aep-and-ari/
        aep = (np.exp(1 / ARI) - 1) / np.exp(1 / ARI)
        return 1 - aep

    def distr_func(self):
        paras = distr.gev.lmom_fit(self.AMS)
        fitted_gev = distr.gev(**paras)
        return fitted_gev

    def pts_on_curve(self):
        ARI_list = np.array([2, 5, 10, 25, 50, 100, 200, 500, 1000])
        prec_depths = self.distr_func().ppf(self.AEP(ARI_list))
        return prec_depths


# Generates a dataframe matching Atlas14 output table format, to be output as a CSV in sample_run
# (either with or without smoothing applied)
#
# Columns = recurrence intervals. Rows = durations.
# The max_100yr argument cuts off durations over 100y.
def output_forCSV(inputs_path, loc='MSP', fromWRF=True, scenario='HIST', model='all',
                  fullrecord=True, init=1837, end=2010,
                  applySmoothing=False, max_100yr=True, multiscen=['no'], n_row_drop=0):
    df = pd.DataFrame(data=None, index=dur_list, columns=ARI_list)
    for duration in dur_list:
        if fromWRF:
            if multiscen[0] == 'no':
                AMSconstrained = AMS_WRF(inputs_path, loc, duration, scenario, model, n_row_drop=n_row_drop)
            else:
                AMSconstrained = MultiScenario(inputs_path, loc, duration, multiscen[0], multiscen[1], model,
                                               n_row_drop=n_row_drop)
        else:
            AMSconstrained = AMS_obsv(inputs_path, loc, duration, fullrecord, init, end)

        df.loc[duration] = GEVfit(AMSconstrained, duration, applyCorrFactor=True).pts_on_curve()

    if applySmoothing:
        for AEP in df.columns:
            spline_func = interpolate.UnivariateSpline(dur_list, df[AEP].values)
            df[AEP] = spline_func(dur_list)
    if max_100yr:
        df.drop(columns=[200, 500, 1000], inplace=True)

    return df
