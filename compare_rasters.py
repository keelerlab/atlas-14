import pygeoprocessing as pg
from os import path
from osgeo.gdalnumeric import *
import gdal
import numpy as np


def align_rasters(noaa_directory, modeled_directory):
    noaa_paths, aligned_noaa_paths = make_noaa_paths(noaa_directory)
    modeled_paths, aligned_modeled_paths = make_modeled_paths(modeled_directory, ['HIST', 'END4.5', 'END8.5'])
    all_paths = noaa_paths + modeled_paths
    all_aligned_paths = aligned_noaa_paths + aligned_modeled_paths
    rs_list = ['bilinear'] * len(all_paths)
    info = pg.get_raster_info(modeled_paths[1])

    pg.align_and_resize_raster_stack(all_paths, all_aligned_paths, rs_list,
                                     (info['pixel_size'][0], info['pixel_size'][1]),
                                     'intersection', raster_align_index=len(all_paths)-1)


def compare_aligned_rasters(noaa_directory, modeled_directory, comparisons_directory):
    modeled_years = [2, 5, 10, 25, 50, 100]
    modeled_durations = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
    ndv = np.nan
    template_path = path.join(modeled_directory, 'aligned', 'HIST', r'HIST_DDF_1d_2yr.tif')

    for year in modeled_years:
        for duration in modeled_durations:
            model_filename = f'DDF_{duration}d_{year}yr.tif'
            noaa_filename = f'NOAA_{duration}d_{year}yr.tif'

            noaa_to_hist_dif_filename = f'NtoH_dif_{duration}d_{year}yr.tif'
            noaa_to_hist_pct_filename = f'NtoH_pct_{duration}d_{year}yr.tif'

            hist_to_END45_dif_filename = f'HtoE45_dif_{duration}d_{year}yr.tif'
            hist_to_END45_pct_filename = f'HtoE45_pct_{duration}d_{year}yr.tif'

            hist_to_END85_dif_filename = f'HtoE85_dif_{duration}d_{year}yr.tif'
            hist_to_END85_pct_filename = f'HtoE85_pct_{duration}d_{year}yr.tif'

            noaa_path = path.join(noaa_directory, 'aligned', noaa_filename)
            hist_path = path.join(modeled_directory, 'aligned', 'HIST', f'HIST_{model_filename}')
            end45_path = path.join(modeled_directory, 'aligned', 'END4.5', f'END4.5_{model_filename}')
            end85_path = path.join(modeled_directory, 'aligned', 'END8.5', f'END8.5_{model_filename}')

            noaa_to_hist_dif = path.join(comparisons_directory, 'NOAA_to_HIST_dif', noaa_to_hist_dif_filename)
            noaa_to_hist_pct = path.join(comparisons_directory, 'NOAA_to_HIST_pct', noaa_to_hist_pct_filename)

            hist_to_END45_dif = path.join(comparisons_directory, 'HIST_to_END4.5_dif', hist_to_END45_dif_filename)
            hist_to_END45_pct = path.join(comparisons_directory, 'HIST_to_END4.5_pct', hist_to_END45_pct_filename)

            hist_to_END85_dif = path.join(comparisons_directory, 'HIST_to_END8.5_dif', hist_to_END85_dif_filename)
            hist_to_END85_pct = path.join(comparisons_directory, 'HIST_to_END8.5_pct', hist_to_END85_pct_filename)

            noaa_arr = raster_to_array(noaa_path, ndv)
            noaa_arr = noaa_arr / 1000  # NOAA stores values as multiple of 1000

            hist_arr = raster_to_array(hist_path, ndv)
            end45_arr = raster_to_array(end45_path, ndv)
            end85_arr = raster_to_array(end85_path, ndv)

            noaa_hist_dif = dif(noaa_arr, hist_arr)
            noaa_hist_pct = pct_dif(noaa_arr, hist_arr)

            hist_end45_dif = dif(hist_arr, end45_arr)
            hist_end45_pct = pct_dif(hist_arr, end45_arr)

            hist_end85_dif = dif(hist_arr, end85_arr)
            hist_end85_pct = pct_dif(hist_arr, end85_arr)

            array_to_gtiff(noaa_hist_dif, template_path, noaa_to_hist_dif, ndv)
            array_to_gtiff(noaa_hist_pct, template_path, noaa_to_hist_pct, ndv)

            array_to_gtiff(hist_end45_dif, template_path, hist_to_END45_dif, ndv)
            array_to_gtiff(hist_end45_pct, template_path, hist_to_END45_pct, ndv)

            array_to_gtiff(hist_end85_dif, template_path, hist_to_END85_dif, ndv)
            array_to_gtiff(hist_end85_pct, template_path, hist_to_END85_pct, ndv)


def make_noaa_paths(noaa_directory):
    noaa_paths = []
    aligned_noaa_paths = []
    noaa_years = [2, 5, 10, 25, 50, 100]
    noaa_durations = ['24', '48', '03', '04', '07', '10', '20', '30', '45', '60']

    for year in noaa_years:
        for duration in noaa_durations:
            if duration in ['24', '48']:
                filename = f'mw{year}yr{duration}ha.tif'
                if duration == '24':
                    duration = 1
                    outfilename = f'NOAA_{duration}d_{year}yr.tif'
                else:
                    duration = 2
                    outfilename = f'NOAA_{duration}d_{year}yr.tif'
            else:
                filename = f'mw{year}yr{duration}da.tif'
                duration = int(duration)
                outfilename = f'NOAA_{duration}d_{year}yr.tif'
            noaa_paths.append(path.join(noaa_directory, 'prj', filename))
            aligned_noaa_paths.append(path.join(noaa_directory, 'aligned', outfilename))

    return noaa_paths, aligned_noaa_paths


def make_modeled_paths(modeled_directory, scenario_list):
    modeled_paths = []
    aligned_modeled_paths = []
    modeled_years = [2, 5, 10, 25, 50, 100]
    modeled_durations = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

    for year in modeled_years:
        for duration in modeled_durations:
            for scenario in scenario_list:
                filename = f'{scenario}_DDF_{duration}d_{year}yr.tif'
                modeled_paths.append(path.join(modeled_directory, scenario, filename))
                aligned_modeled_paths.append(path.join(modeled_directory, 'aligned', scenario, filename))

    return modeled_paths, aligned_modeled_paths


def raster_to_array(gdal_raster, out_ndv):
    ds = gdal.Open(gdal_raster)
    band = ds.GetRasterBand(1)
    in_ndv = band.GetNoDataValue()
    raster_array = BandReadAsArray(band)
    raster_array = raster_array.astype(float)
    raster_array[raster_array == in_ndv] = out_ndv
    return raster_array


def pct_dif(reference_array, comparison_array):
    pct_dif_array = (comparison_array - reference_array) / reference_array
    return pct_dif_array


def dif(reference_array, comparison_array):
    dif_array = comparison_array - reference_array
    return dif_array


def array_to_gtiff(in_arrary, template_path, out_name, ndv):
    template = gdal.Open(template_path)
    x_size = template.RasterXSize
    y_size = template.RasterYSize
    out_raster = gdal.GetDriverByName('GTiff').Create(out_name, x_size, y_size, 1, gdal.GDT_Float32,
                                                      options=['COMPRESS=LZW'])
    out_raster.SetGeoTransform(template.GetGeoTransform())
    out_raster.SetProjection(template.GetProjection())
    band = out_raster.GetRasterBand(1)
    band.SetNoDataValue(ndv)
    BandWriteArray(band, in_arrary)
    out_raster = None
    band = None


comparisons_path = 'C:/Users/rrnoe/Work/Stormwater/v3/outputs'
modeled_DDF_grids_path = 'C:/Users/rrnoe/Work/Stormwater/v3/modeled_DDF_grids'
NOAA_grids_path = 'C:/Users/rrnoe/Work/Stormwater/v3/noaa_grids'


align_rasters(NOAA_grids_path, modeled_DDF_grids_path)
compare_aligned_rasters(NOAA_grids_path, modeled_DDF_grids_path, comparisons_path)




