import os
import rasterio
import pandas as pd
from rasterstats import zonal_stats
import geopandas as gpd

years = [2, 5, 10, 25, 50, 100]
durs = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]


def zs(shpfile, ras_path, prefix):
    for year in years:
        for duration in durs:
            filename = f'{prefix}_{duration}d_{year}yr.tif'
            fullpath = os.path.join(ras_path, filename)
            print(filename)
            colname = f'm{duration}d_{year}y'
            temp_df = pd.DataFrame(zonal_stats(shpfile, fullpath, stats='mean', all_touched=False))
            temp_df['mean'] = temp_df['mean']*100
            temp_df.rename(columns={'mean': colname}, inplace=True)
            shpfile = shpfile.join(temp_df)
    return shpfile


shp = gpd.read_file(r"C:\Users\rrnoe\Work\Stormwater\v3\aux_data\regions_wgs1984.shp")
# shp = shp[['COUNTYNAME', 'geometry']]
shp = shp[['FEATURE_NA', 'geometry']]

# outshp = zs(shp, 'C:/Users/rrnoe/Work/Stormwater/v3/outputs/HIST_to_END8.5_pct', 'HtoE85_pct')
outshp = zs(shp, 'C:/Users/rrnoe/Work/Stormwater/v3/outputs/NOAA_to_HIST_pct', 'NtoH_pct')

outcsv = outshp.drop('geometry', axis=1)
outcsv.to_csv('c:/Users/rrnoe/Desktop/region_HtoN_pct.csv', index=False)


