import pandas as pd
from os import path
import AMS_to_DDF as DDF
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from rasterstats import zonal_stats
import geopandas as gpd
import numpy as np


def make_ddf_figure(ax, df, location, scenario, durmin=1, durmax=60):
    rows = list(df.index.values)
    durs = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

    flipped = np.flip(df.columns.values)
    # for AEP in df.columns:
    for AEP in flipped:
        depths = df.loc[durmin:durmax, AEP].values
        ax.semilogx(durs, depths)



    plt.title('{0} County ({1})'.format(location, scenario))
    plt.xlabel('Duration (days)')
    plt.ylabel('Precipitation depth (inches)')
    legendstr = 'Annual \nexceedance \nprobability \n(1/years)'
    plt.legend(flipped, title=legendstr, loc=0, bbox_to_anchor=(1, 0.9))
    plt.xticks(ticks=durs, labels=durs)
    plt.xlim(durmin, durmax)
    plt.grid(linestyle='--')


def make_pf_table(ax, df, edge_color='grey', edge_width=0.5):
    columns = ('2', '5', '10', '25', '50', '100')
    row_headers = [' 1 ', ' 2 ', ' 3 ', ' 4 ', ' 7 ', ' 10 ', ' 20 ', ' 30 ', ' 45 ', ' 60 ']

    rcolors = ['#CCD9C7'] * len(row_headers)
    ccolors = ['#CCD9C7'] * len(columns)

    pf_table = ax.table(cellText=df.values, rowLabels=row_headers, rowColours=rcolors, rowLoc='center',
                        colLabels=columns, colColours=ccolors, colWidths=[0.1666] * 6, loc=9)
    ax.yaxis.set_label_coords(-0.055, 0.5)
    # pf_table.auto_set_font_size(False)
    # pf_table.set_fontsize(9)

    ax.set_frame_on(False)
    ax.set_title('Modeled Precipition Depth Estimates Difference (%) | End Century Depth (inches)')
    ax.set_xlabel('Average recurrence interval - years')
    ax.set_ylabel('Duration - days')
    ax.xaxis.set_label_position('top')
    ax.axes.xaxis.set_ticks([])
    ax.axes.yaxis.set_ticks([])

    for key, cell in pf_table.get_celld().items():
        cell.set_edgecolor(edge_color)
        cell.set_linewidth(edge_width)
        # cell.set_fontsize(24)
        cell.set_height(1/(len(df)+1))   # 1 / number of rows in table plus the header


def colormap(val):
    if val >= 60:
        return '#3182bd'
    elif 30 <= val < 60:
        return '#9ecae1'
    elif 15 <= val < 30:
        return '#deebf7'
    elif 15 > val > -15:
        return 'white'
    elif -15 >= val > -30:
        return '#fee6ce'
    elif -30 >= val > -60:
        return '#fdae6b'
    elif val <= -60:
        return '#e6550d'


def make_color_pf_table(ax, df, condf, edge_color='grey', edge_width=0.5):
    columns = ('2', '5', '10', '25', '50', '100')
    row_headers = [' 1 ', ' 2 ', ' 3 ', ' 4 ', ' 7 ', ' 10 ', ' 20 ', ' 30 ', ' 45 ', ' 60 ']
    rcolors = ['#CCD9C7'] * len(row_headers)
    ccolors = ['#CCD9C7'] * len(columns)

    colors = condf.applymap(lambda x: colormap(x))

    pf_table = ax.table(cellText=df.values, rowLabels=row_headers, rowColours=rcolors, rowLoc='center',
                        colLabels=columns, colColours=ccolors, colWidths=[0.1666] * 6, cellColours=colors.to_numpy(), loc=9)
    ax.yaxis.set_label_coords(-0.055, 0.5)
    # pf_table.auto_set_font_size(False)
    # pf_table.set_fontsize(9)

    ax.set_frame_on(False)
    ax.set_title('Differences Between NOAA and Modeled Historic Precipitation Depth Estimates - inches')
    ax.set_xlabel('Average recurrence interval - years')
    ax.set_ylabel('Duration - days')
    ax.xaxis.set_label_position('top')
    ax.axes.xaxis.set_ticks([])
    ax.axes.yaxis.set_ticks([])

    for key, cell in pf_table.get_celld().items():
        cell.set_edgecolor(edge_color)
        cell.set_linewidth(edge_width)
        # cell.set_fontsize(24)
        cell.set_height(1/(len(df)+1))   # 1 / number of rows in table plus the header


def make_combined_pf_df(df, pct_df):
    combined = pd.DataFrame()
    years = [2, 5, 10, 25, 50, 100]
    for year in years:
        combined[year] = pct_df[year].map(str)+'% ' + '('+df[year].map(str).str.ljust(4, fillchar='0')+')'
    return combined


def make_combined_pf_df2(df, abs_df, pct_df):
    combined = pd.DataFrame()
    years = [2, 5, 10, 25, 50, 100]
    for year in years:
        combined[year] = df[year].map(str) + '\n(' + abs_df[year].map(str) + ')' + '\n' + pct_df[year].map(str) + '%'
    return combined


def reshape_cached_data(df, location, subset):
    years = [2, 5, 10, 25, 50, 100]
    durs = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

    df = df.loc[df['COUNTYNAME'] == location].reset_index()
    rows = []

    for dur in durs:
        row = []
        for year in years:
            colname = f'{subset}_{dur}d_{year}yr'
            val = df.at[0, colname]
            row.append(val)
        rows.append(row)

    reshaped = pd.DataFrame(rows, columns=years)
    reshaped.index = durs
    return reshaped


def create_cached_data(shp, folder_dict, outcsv, outpath):
    years = [2, 5, 10, 25, 50, 100]
    durations = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
    for folder in folder_dict:
        for year in years:
            for duration in durations:
                filename = f'{folder_dict[folder]}_{duration}d_{year}yr.tif'
                print(filename)
                raster_path = path.join(folder, filename)
                temp_df = pd.DataFrame(zonal_stats(shp, raster_path, stats='mean', all_touched=False))
                temp_df.rename(columns={'mean': filename[:-4]}, inplace=True)
                outcsv = outcsv.join(temp_df)
    outcsv.to_csv(outpath)


# loc_list = ['Mower', 'Douglas']
loc_list = ['Aitkin', 'Anoka', 'Becker', 'Beltrami', 'Benton', 'Big Stone', 'Blue Earth', 'Brown', 'Carlton', 'Carver',
            'Cass', 'Chippewa', 'Chisago', 'Clay', 'Clearwater', 'Cook', 'Cottonwood', 'Crow Wing', 'Dakota', 'Dodge',
            'Douglas', 'Faribault', 'Fillmore', 'Freeborn', 'Goodhue', 'Grant', 'Hennepin', 'Houston', 'Hubbard',
            'Isanti', 'Itasca', 'Jackson', 'Kanabec', 'Kandiyohi', 'Kittson', 'Koochiching', 'Lac qui Parle', 'Lake',
            'Lake of the Woods', 'Le Sueur', 'Lincoln', 'Lyon', 'McLeod', 'Mahnomen', 'Marshall', 'Martin', 'Meeker',
            'Mille Lacs', 'Morrison', 'Mower', 'Murray', 'Nicollet', 'Nobles', 'Norman', 'Olmsted', 'Otter Tail',
            'Pennington', 'Pine', 'Pipestone', 'Polk', 'Pope', 'Ramsey', 'Red Lake', 'Redwood', 'Renville', 'Rice',
            'Rock', 'Roseau', 'St. Louis', 'Scott', 'Sherburne', 'Sibley', 'Stearns', 'Steele', 'Stevens', 'Swift',
            'Todd', 'Traverse', 'Wabasha', 'Wadena', 'Waseca', 'Washington', 'Watonwan', 'Wilkin', 'Winona', 'Wright',
            'Yellow Medicine']
use_cached = False

shp = gpd.read_file('C:/Users/rrnoe/Work/Stormwater/v2/county_wgs84.shp')
shp_csv = shp[['COUNTYNAME', 'fcode']]
cache_outpath = 'C:/Users/rrnoe/Work/Stormwater/v3/results_cache_WO3day_SD.csv'
folder_paths = {'C:/Users/rrnoe/Work/Stormwater/v3/outputs/NOAA_to_HIST_dif': 'NtoH_dif',
               'C:/Users/rrnoe/Work/Stormwater/v3/outputs/NOAA_to_HIST_pct': 'NtoH_pct',
               'C:/Users/rrnoe/Work/Stormwater/v3/outputs/HIST_to_END4.5_pct': 'HtoE45_pct',
               'C:/Users/rrnoe/Work/Stormwater/v3/outputs/HIST_to_END8.5_pct': 'HtoE85_pct',
               'C:/Users/rrnoe/Work/Stormwater/v3/modeled_DDF_grids/aligned/HIST': 'HIST_DDF',
               'C:/Users/rrnoe/Work/Stormwater/v3/modeled_DDF_grids/aligned/END4.5': 'END4.5_DDF',
               'C:/Users/rrnoe/Work/Stormwater/v3/modeled_DDF_grids/aligned/END8.5': 'END8.5_DDF',
               'C:/Users/rrnoe/Work/Stormwater/v3/NOAA_grids/aligned': 'NOAA'
               }

if not use_cached:
    create_cached_data(shp, folder_paths, shp_csv, cache_outpath)

data = pd.read_csv(cache_outpath)

for loc in loc_list:
    hist_absdif_noaa = reshape_cached_data(data, loc, 'NtoH_dif').round(decimals=2)
    hist_pctdif_noaa = (reshape_cached_data(data, loc, 'NtoH_pct')*100).round(decimals=1)
    hist = reshape_cached_data(data, loc, 'HIST_DDF').round(decimals=2)
    hist_page = [hist, 'na', 'na', hist_absdif_noaa, hist_pctdif_noaa, 'HIST 1980-1997']

    end85_pctdif_hist = (reshape_cached_data(data, loc, 'HtoE85_pct')*100).round(decimals=1)
    end85 = reshape_cached_data(data, loc, 'END8.5_DDF').round(decimals=2)
    end85_page = [end85, 'na', end85_pctdif_hist, 'na', 'na', '2080-2097 High Emissions']

    # NOAA data are still multiplied by 1000 in the cached data.

    pages = [end85_page]
    table_pages = [hist_page]

    with PdfPages(f'c:/users/rrnoe/desktop/counties/{loc}.pdf') as pdf:
        ########################################### Page 1 ###################################################
        for page in pages:
            fig = plt.figure(figsize=(8.5, 11), dpi=300)

            ax1 = fig.add_subplot(211)
            make_ddf_figure(ax1, page[0], loc, page[5])
            cb = ax1.get_position()
            ax1.set_position([cb.x0, 0.66, cb.width * 0.85, cb.height * 0.72])

            t2 = ("Projected precipitation depths for 2080-2097 under high emissions scenario (RCP 8.5). These curves were\n"
                 "created from climate change projections using methods consistent with those used to create NOAA Atlas-14.\n"
                 "However, these estimates should be interpreted carefully because they are based on a 17-year annual\n"
                 "maximum series, while NOAA estimates typically use observations from at least 30 years, and sometimes\n"
                 "more than 100. Despite a limited series, our modeled 1980-1997 data showed favorable agreement with\n"
                 "NOAA estimates for many durations, frequencies, and locations. See tables below and final report for\n"
                 "strategies for interpreting these estimates that consider the limitations of the underlying data.")

            # plt.text(-0.04, -0.05, t2, fontsize=9.5, va='bottom', ha='left')
            plt.text(0.7, -3.5, t2, fontsize=9.5, va='top', ha='left')

            ax2 = fig.add_subplot(212)
            combined_df = make_combined_pf_df(page[0], page[2])

            make_pf_table(ax2, combined_df)
            cb = ax2.get_position()
            ax2.set_position([cb.x0, 0.2, cb.width, cb.height * 0.72])

            t = ("Percent change between our modeled 1980-1997 and 2080-2097 estimates, with precipitation depth for\n"
                 "2080-2097 estimates shown in parentheses. The preferred way to interpret climate change projections\n"
                 "is to compare a modeled historical scenario with a future scenario, however, also consider the error\n"
                 "for the duration-recurrence interval combination of interest shown in the table below. Observed and\n"
                 "modeled precipitation depths and the relative and aboslute differences between them should all be \n"
                 "considered when interpreting these data.")


            plt.text(-0.076, -0.05, t, fontsize=9.5, va='top', ha='left')
            # plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)

            pdf.savefig()
            plt.close()
        ########################################### Page 2 ###################################################
        for page in table_pages:
            fig = plt.figure(figsize=(8.5, 11), dpi=300)

            ax2 = fig.add_subplot(111)
            combined_df = make_combined_pf_df2(page[0], page[3], page[4])
            make_color_pf_table(ax2, combined_df, page[4])
            cb = ax2.get_position()
            ax2.set_position([cb.x0, 0.3, cb.width, cb.height * 0.72])

            t = ("Differences between our modeled historical data and NOAA estimates. The first line in each cell is the\n"
                 "precipitation depth estimate based on modeled 1980-1997 data. The second line is the difference in\n"
                 "inches between our estimate and NOAA. The third line is the percent difference between the estimates.\n"
                 "Differences between -15% and 15% are shaded white, 15% to 30% light blue, 30% to 60% blue,\n"
                 "and >60% dark blue. The shading intervals are reversed using shades of orange for negative\n"
                 "values. These values indicate how well our data and methods were able to re-create NOAA estimates\n"
                 "using a much shorter time series (1980-1997) of modeled data. However, because our modeled data \n"
                 "represents the end of the 20th century, some differences between the estimates may be due to climate\n"
                 "change. Users that intend to use modeled precipitation depths rather than relative differences should\n"
                 "consult this table to check how well our projections performed for the county, duration, and recurrence\n"
                 "interval of interest. These values can also be used to adjust projected values.")

            plt.text(-0.04, -0.05, t, fontsize=9.5, va='top', ha='left', wrap=True)

            pdf.savefig()
            plt.close()

