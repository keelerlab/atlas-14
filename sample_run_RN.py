from pathlib import Path
import os
import wget
import rasterio
from rasterio.warp import calculate_default_transform, reproject, Resampling
import rasterio.mask
import fiona
import pandas as pd
from rasterstats import zonal_stats
import geopandas as gpd
import AMS_grid


years = ['2', '5', '10', '25', '50', '100']
durs = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
adurs = ['24', '48', '03', '04', '07', '10', '20', '30', '45', '60']

counties = ['Aitkin', 'Anoka', 'Becker', 'Beltrami', 'Benton', 'Big Stone', 'Blue Earth', 'Brown', 'Carlton', 'Carver',
            'Cass', 'Chippewa', 'Chisago', 'Clay', 'Clearwater', 'Cook', 'Cottonwood', 'Crow Wing', 'Dakota', 'Dodge',
            'Douglas', 'Faribault', 'Fillmore', 'Freeborn', 'Goodhue', 'Grant', 'Hennepin', 'Houston', 'Hubbard',
            'Isanti', 'Itasca', 'Jackson', 'Kanabec', 'Kandiyohi', 'Kittson', 'Koochiching', 'Lac qui Parle', 'Lake',
            'Lake of the Woods', 'Le Sueur', 'Lincoln', 'Lyon', 'McLeod', 'Mahnomen', 'Marshall', 'Martin', 'Meeker',
            'Mille Lacs', 'Morrison', 'Mower', 'Murray', 'Nicollet', 'Nobles', 'Norman', 'Olmsted', 'Otter Tail',
            'Pennington', 'Pine', 'Pipestone', 'Polk', 'Pope', 'Ramsey', 'Red Lake', 'Redwood', 'Renville', 'Rice',
            'Rock', 'Roseau', 'St. Louis', 'Scott', 'Sherburne', 'Sibley', 'Stearns', 'Steele', 'Stevens', 'Swift',
            'Todd', 'Traverse', 'Wabasha', 'Wadena', 'Waseca', 'Washington', 'Watonwan', 'Wilkin', 'Winona', 'Wright',
            'Yellow Medicine']

inputs_path = 'C:/Users/rrnoe/Work/Stormwater/v2/'
base_outpath = 'C:/Users/rrnoe/desktop/test/'
extracted_path = 'C:/Users/rrnoe/Desktop/extracted/'
prj_path = 'C:/Users/rrnoe/Desktop/prj/'
clip_path = 'C:/Users/rrnoe/Work/Stormwater/v2/noaa_grids/clip/'

shp_path = r"C:\Users\rrnoe\Work\Stormwater\v2\county_wgs84.shp"


def clip_raster(fp, shppath, outpath):
    srcRst = rasterio.open(fp)

    with fiona.open(shppath, "r") as shapefile:
        shapes = [feature["geometry"] for feature in shapefile]

    out_image, out_transform = rasterio.mask.mask(srcRst, shapes, crop=True)
    out_meta = srcRst.meta

    out_meta.update({"driver": "GTiff",
                     "height": out_image.shape[1],
                     "width": out_image.shape[2],
                     'compress': 'LZW',
                     "transform": out_transform})

    with rasterio.open(outpath, "w", **out_meta) as dest:
        dest.write(out_image)


def reproject_raster(fp, outEPSG, outpath):
    srcRst = rasterio.open(fp)

    dstCrs = {'init': outEPSG}

    # calculate transform array and shape of reprojected raster
    transform, width, height = calculate_default_transform(srcRst.crs, dstCrs, srcRst.width, srcRst.height,
                                                           *srcRst.bounds)

    # the meta for the destination raster
    kwargs = srcRst.meta.copy()
    # kwargs = out_image.meta.copy()
    kwargs.update({
        'driver': 'GTiff',
        # 'nodata': ndv,
        # 'dtype': rasterio.uint16,
        'compress': 'LZW',
        'crs': dstCrs,
        'transform': transform,
        'width': width,
        'height': height
    })

    # open destination raster
    dstRst = rasterio.open(outpath, 'w', **kwargs)

    # reproject and save raster band data
    reproject(source=rasterio.band(srcRst, 1), destination=rasterio.band(dstRst, 1), src_crs=srcRst.crs, dst_crs=dstCrs,
              resampling=Resampling.nearest)

    dstRst.close()


def get_noaa_data():
    base_url = 'https://hdsc.nws.noaa.gov/pub/hdsc/data/mw/'
    for year in years:
        for duration in durations:
            if duration in ['24', '48']:
                filename = 'mw' + year + 'yr' + duration + 'ha.zip'
            else:
                filename = 'mw' + year + 'yr' + duration + 'da.zip'
            url = base_url + filename
            print(url)
            wget.download(url, out=base_outpath + filename)


def reproject_wrapper():
    for year in years:
        for duration in durations:
            if duration in ['24', '48']:
                filename = 'mw' + year + 'yr' + duration + 'ha'
                path = extracted_path + filename + '/' + filename + '.asc'
            else:
                filename = 'mw' + year + 'yr' + duration + 'da'
                path = extracted_path + filename + '/' + filename + '.asc'

            print(filename)
            outpath = 'C:/Users/rrnoe/Desktop/prj/' + filename + '.tif'
            reproject_raster(path, 'EPSG:4326', outpath)


def clip_wrapper():
    for year in years:
        for duration in durations:
            if duration in ['24', '48']:
                filename = 'mw' + year + 'yr' + duration + 'ha'
                path = prj_path + filename + '.tif'
            else:
                filename = 'mw' + year + 'yr' + duration + 'da'
                path = prj_path + filename + '.tif'

            print(filename)
            outpath = 'C:/Users/rrnoe/Desktop/clip/' + filename + '.tif'
            clip_raster(path, shp, outpath)


def make_county_DDF_df(scenario):
    cdf = pd.DataFrame()
    if scenario == 'HIST':
        prefix = 'h'
    else:
        prefix = 'm'
    for county in counties:

        filepath = os.path.join(inputs_path,
                                f'countyDDF_new_aep/county{length}yr/DDF_fromWRF outputs/{scenario}/{county}/all_DDF_smoothed.csv')
        df = pd.read_csv(filepath, index_col=0)
        rowdict = {'county': county}
        for yr in years:
            for dur in durs:
                newcol = f'{prefix}{dur:02}d{yr}y'
                val = df.at[dur, yr]
                rowdict[newcol] = val
        cdf = cdf.append(rowdict, ignore_index=True)
    return cdf


def pct_dif_from_noaa(mdf, shp, use_cached=False):
    if use_cached:
        shp = gpd.read_file(r'C:\Users\rrnoe\Work\Stormwater\v2\noaa_grids\county_cache.shp')
    else:
        for year in years:
            for duration in adurs:
                if duration in ['24', '48']:
                    filename = 'mw' + year + 'yr' + duration + 'ha'
                    path = clip_path + filename + '.tif'
                    if duration == '24':
                        duration = '01'
                    elif duration == '48':
                        duration = '02'
                else:
                    filename = 'mw' + year + 'yr' + duration + 'da'
                    path = clip_path + filename + '.tif'
                print(filename)
                colname = f'a{duration}d{year}y'
                temp_df = pd.DataFrame(zonal_stats(shp, path, stats='mean', all_touched=False))
                temp_df['mean'] = temp_df['mean'] / 1000
                temp_df.rename(columns={'mean': colname}, inplace=True)
                shp = shp.join(temp_df)
                shp.to_file(r'C:\Users\rrnoe\Work\Stormwater\v2\noaa_grids\county_cache.shp')

    shp = shp.join(mdf)


    keepcols = ['COUNTYNAME', 'geometry']
    for year in years:
        for dur in durs:
            col = f'{dur:02}d{year}y'
            shp[f'd{col}'] = (((shp[f'm{col}'] - shp[f'a{col}']) / shp[f'a{col}']) * 100).round(decimals=1)
            keepcols.append(f'd{col}')

    shp = shp[keepcols]
    return shp

def county_pct_dif_from_hist(mdf, mdf_hist):
    df = pd.merge(mdf_hist, mdf, on='county')
    keepcols = ['county']
    for year in years:
        for dur in durs:
            col = f'{dur:02}d{year}y'
            df[f'd{col}'] = (((df[f'm{col}'] - df[f'h{col}']) / df[f'h{col}']) * 100).round(decimals=1)
            keepcols.append(f'd{col}')

    df = df[keepcols]
    return df


def pct_dif_from_noaa_zs(shp, AMS_length, scenario, use_cached=False):
    if use_cached:
        shp = gpd.read_file(r'C:\Users\rrnoe\Work\Stormwater\v2\noaa_grids\county_cache.shp')
    else:
        for year in years:
            for duration in adurs:
                if duration in ['24', '48']:
                    filename = 'mw' + year + 'yr' + duration + 'ha'
                    path = clip_path + filename + '.tif'
                    if duration == '24':
                        duration = '01'
                    elif duration == '48':
                        duration = '02'
                else:
                    filename = 'mw' + year + 'yr' + duration + 'da'
                    path = clip_path + filename + '.tif'
                print(filename)
                colname = f'a{duration}d{year}y'
                temp_df = pd.DataFrame(zonal_stats(shp, path, stats='mean', all_touched=False))
                temp_df['mean'] = temp_df['mean'] / 1000
                temp_df.rename(columns={'mean': colname}, inplace=True)
                shp = shp.join(temp_df)
                shp.to_file(r'C:\Users\rrnoe\Work\Stormwater\v2\noaa_grids\county_cache.shp')

    for year in years:
        for duration in durs:
            filename = f'DDF_IM_{AMS_length}_{duration}d_{year}yr.tif'
            path = os.path.join(inputs_path,'DDF_grids_IM', scenario, 'ensemble_DDF', filename)
            print(filename)
            colname = f'm{duration:02}d{year}y'
            temp_df = pd.DataFrame(zonal_stats(shp, path, stats='mean', all_touched=False))
            temp_df.rename(columns={'mean': colname}, inplace=True)
            shp = shp.join(temp_df)

    keepcols = ['COUNTYNAME', 'geometry']

    for year in years:
        for dur in durs:
            col = f'{dur:02}d{year}y'
            shp[f'd{col}'] = (((shp[f'm{col}'] - shp[f'a{col}']) / shp[f'a{col}']) * 100).round(decimals=1)
            keepcols.append(f'd{col}')

    shp = shp[keepcols]
    return shp


def pct_dif_from_hist_zs(shp, AMS_length, scenario):
    for year in years:
        for duration in durs:
            filename = f'DDF_IM_{AMS_length}_{duration}d_{year}yr.tif'
            path = os.path.join(inputs_path, 'DDF_grids_IM', 'HIST', 'ensemble_DDF', filename)
            print(filename)
            colname = f'h{duration:02}d{year}y'
            temp_df = pd.DataFrame(zonal_stats(shp, path, stats='mean', all_touched=False))
            temp_df.rename(columns={'mean': colname}, inplace=True)
            shp = shp.join(temp_df)

    for year in years:
        for duration in durs:
            filename = f'DDF_IM_{AMS_length}_{duration}d_{year}yr.tif'
            path = os.path.join(inputs_path,'DDF_grids_IM', scenario, 'ensemble_DDF', filename)
            print(filename)
            colname = f'm{duration:02}d{year}y'
            temp_df = pd.DataFrame(zonal_stats(shp, path, stats='mean', all_touched=False))
            temp_df.rename(columns={'mean': colname}, inplace=True)
            shp = shp.join(temp_df)

    keepcols = ['COUNTYNAME', 'geometry']

    for year in years:
        for dur in durs:
            col = f'{dur:02}d{year}y'
            shp[f'd{col}'] = (((shp[f'm{col}'] - shp[f'h{col}']) / shp[f'h{col}']) * 100).round(decimals=1)
            keepcols.append(f'd{col}')

    shp = shp[keepcols]
    return shp

scen = 'END8.5'
noaa_scen = 'HIST'
# scenarios_list = ['END4.5']
county_shp = gpd.read_file(shp_path)

row_drop_list = [3]
for n in row_drop_list:
    length = 20 - n
    # modeled_hist_df = make_county_DDF_df('HIST')
    # modeled_df = make_county_DDF_df(scen)
    # county_pctdif_csv = county_pct_dif_from_hist(modeled_df, modeled_hist_df)
    # noaa_comparison_shp = pct_dif_from_noaa(modeled_df, county_shp, use_cached=True)

    comparison_shp = pct_dif_from_hist_zs(county_shp, length, scen)
    comparison_shp.to_file(f'c:/users/rrnoe/desktop/{length}_yr_hist_end85.shp')
    comparison_csv = comparison_shp.drop('geometry', axis=1)
    comparison_csv.to_csv(f'c:/users/rrnoe/desktop/{length}yr_pctDif_{scen}_HIST_IM.csv', index=False)

    comparison_shp = pct_dif_from_noaa_zs(county_shp, length, noaa_scen, use_cached=True)
    comparison_shp.to_file(f'c:/users/rrnoe/desktop/{length}_yr_{noaa_scen}_NOAA.shp')
    comparison_csv = comparison_shp.drop('geometry', axis=1)
    comparison_csv.to_csv(f'c:/users/rrnoe/desktop/{length}yr_pctDif_{noaa_scen}_NOAA_IM.csv', index=False)


    # noaa_comparison_csv.to_csv(f'c:/users/rrnoe/desktop/{length}yr_pctDif_{scen}_HIST_IM.csv', index=False)
    # county_pctdif_csv.to_csv(f'c:/users/rrnoe/desktop/{length}yr_pctDif_{scen}_HIST_new_aep_county-level.csv', index=False)




