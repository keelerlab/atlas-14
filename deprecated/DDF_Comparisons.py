'''
Compares tabular outputs between model scenarios, or HIST and observed (official Atlas-14
values or non-regiionalized, single-station-based values calculated here).
Comparisons expressed as percentages.
    
Heatmap function gives a visual representation of this comparison on a 2d basis.

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import os
from pathlib import Path


scenario_dict = {'HIST', 'MID', 'END4.5', 'END8.5'}
model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}

    
# The 2 files have to be formatted the same 
#  (either both or neither cut off reccurrence intervals past 100y, etc.)  
# This is currently set up with the assumption that all row and column names match.
def compare(file1, file2):
    df1, df2 = pd.read_csv(file1, index_col=0), pd.read_csv(file2, index_col=0)
        
    percent_chg = ((df2 - df1) / df1) * 100
    #percent_chg.index, percent_chg.columns = df1.index, df1.columns
    return percent_chg


def heatmap(file1, file2, fig_outpath, loc, scen1_str, scen2_str):
   
    df = compare(file1, file2)
    
    plt.title('Change from {0} to {1} ({2} County)'.format(scen1_str, scen2_str, loc))
    sidebar_str = 'Change in precipitation depth (%)'
    VMin = -30
    VMax = 30

    df = df.astype(int)
    sb.heatmap(df, vmin=VMin, vmax=VMax, cmap ='RdYlBu', linewidths = 0.30, 
               annot = True, fmt='d', cbar_kws={'label': sidebar_str})
    plt.xlabel('Annual exceedance probability (1/years)')
    plt.ylabel('Duration (days)')
    plt.yticks(rotation=0)
    
    fig_outpath_full = os.path.join(fig_outpath,
                                    '{0}_to_{1}_heatmap.png'.format(scen1_str, scen2_str))  
    plt.savefig(fig_outpath_full, dpi=150)
    plt.close()
    
    '''
    plt.imshow(df, cmap ="RdYlBu") 
    plt.colorbar()
    plt.xticks(range(len(df)), df.columns) 
    plt.yticks(range(len(df)), df.index) 
    plt.show()
    '''

    