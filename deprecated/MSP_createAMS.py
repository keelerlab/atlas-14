'''
Generates annual max precip series in a usable format for atlas14/lmoments calculations.

'''

import os
import numpy as np
import pandas as pd
import datetime as dt


model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}
scenario_dict = {'HIST':'historical', 'MID':'RCP4.5', 'END4.5':'RCP4.5', 'END8.5':'RCP8.5'}
year_dict = {'HIST':[1980,1999], 'MID':[2040,2059], 'END4.5':[2080,2099], 'END8.5':[2080,2099]}
var_dict = {'temp':'T2_biascorrected', 'prcp':'precip_biascorrected'}

# create dataframe from single-var ensemble csv 
def Input(scenario='HIST', var='prcp'):
    Scen = scenario_dict[scenario]
    dirc = '/Users/birke111/Documents/Hourly_CSV/all_models_{}'.format(Scen)
    inputfile = os.path.join(dirc, 'ensemble_{0}_{1}.csv'.format(scenario, var))
    
    df = pd.read_csv(inputfile, index_col=0)
    return df

# columns needed: year, dailysum
def dailyDF(scenario='HIST', var='prcp'):
    df = Input(scenario, var)
    df['Date'] = [dt.datetime.strptime(Date, '%Y-%m-%d %H:%M:%S').date() for Date in df.DateTime]
    
    # df['DaysCumulative'] = dates - dates[0]
    # df['DaysCumulative'] = df['DaysCumulative'].dt.days
    
    df = df.groupby('Date').agg('sum')
    return df

def AnnualMax(scenario='HIST', var='prcp', duration=1):
    df = dailyDF(scenario, var)
    df['Year'] = [date.year for date in df.index]
    
    df['CumSum'] = np.zeros(len(df), dtype='float')
    for a in range(len(df)):
        df.CumSum[a] = sum(df.ensemble_prcp[a : a+duration])
    df = df.groupby('Year').agg('max')
    
    return df

# calculate AMS from durations not restricted to 12am-12am
# reconfigure a14lm to skip correction factors when pulling from these
def AMSfromHourly(scenario='HIST', var='prcp', duration=1):
    df = Input(scenario, var) 
    df['Date'] = [dt.datetime.strptime(Date, '%Y-%m-%d %H:%M:%S').date() for Date in df.DateTime]
    
    dur_hrs = duration * 24
    df['CumSum'] = np.zeros(len(df), dtype='float')
    for a in range(len(df)):
        df.CumSum[a] = sum(df.ensemble_prcp[a : a+dur_hrs])       
    
    df['Year'] = [Date.year for Date in df.Date]
    df = df.groupby('Year').agg('max')
    
    return df.CumSum
    
    
# combine all durations into one df-to-csv
def CSVoutput(scenario='HIST', fromHourly=True):
    dur_list = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
    #durlabels = [str(a) + 'd' for a in dur_list]
    df = pd.DataFrame()
    
    if fromHourly:
        filename = 'ensemble_{}_AMSfromHourly.csv'.format(scenario)
        for dur in dur_list:
            df[dur] = AMSfromHourly(scenario, 'prcp', dur).values
    else:
        filename = 'ensemble_{}_AMS.csv'.format(scenario)
        for dur in dur_list:
            df[dur] = AnnualMax(scenario, 'prcp', dur).CumSum.values
    
    df.index = AnnualMax(scenario, 'prcp', 1).index
    df.columns = [str(a) + 'd' for a in dur_list]
    
    Scen = scenario_dict[scenario]
    dirc = '/Users/birke111/Documents/Hourly_CSV/all_models_{}'.format(Scen)
    outpath = os.path.join(dirc, filename)
    df.to_csv(outpath)

    

