'''
This calculates hourly average values for a single variable (like precip) across
the 8-model ensemble. These outputs are saved as CSVs, which can be accessed for other
applications without needing to repeat the time-consuming calculations performed here.

For now: this is set up to just calculate hourly averages and leave in that format.
Later: add daily features (precip sums, tmin/tmax - which could be offset for EHF)
'''


import os
import numpy as np
import pandas as pd
import datetime as dt


model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}
scenario_dict = {'HIST':'historical', 'MID':'RCP4.5', 'END4.5':'RCP4.5', 'END8.5':'RCP8.5'}
filename_dict = {'HIST':'TC4', 'MID':'TC45', 'END4.5':'TC89', 'END8.5':'TC4'}

year_dict = {'HIST':[1980,1999], 'MID':[2040,2059], 'END4.5':[2080,2099], 'END8.5':[2080,2099]}
var_dict = {'temp':'T2_biascorrected', 'prcp':'precip_biascorrected', 'RH':'RH'}


# create dataframe from single model csv    
def Input(scenario='HIST', model='BC', var='all'):
    Scen = scenario_dict[scenario]
    Model = model_dict[model]
    dirc = '/Users/birke111/Documents/Hourly_CSV/all_models_{}'.format(Scen)
 
    inputfile = '{0}_{1}_hourly{2}.csv'.format(Model, Scen, filename_dict[scenario])
    fullpath = os.path.join(dirc, Model, Scen, inputfile)
    
    col_names = pd.read_csv(fullpath, nrows=0).columns
    types_dict = {'Date': str}
    types_dict.update({col: float for col in col_names if col not in types_dict})
    df = pd.read_csv(fullpath, dtype=types_dict)
    '''
    
    df = pd.read_csv(fullpath, dtype=str)
    col_names = df.columns
    types_dict = {'Date': str}
    types_dict.update({col: float for col in col_names if col not in types_dict})
    #for col, col_type in types_dict.items():
    #    df[col] = df[col].astype(col_type)
    
    return df
    '''
    
    if var != 'all':
        variable = var_dict[var]
        df = df.loc[:,['Date', 'Time', variable]]
            
    df['Year'] = np.zeros(len(df), dtype='int')
    df['DateTime'] = np.zeros(len(df), dtype=object)
    
    for a in range(len(df.Date.values)):
        try:            
            date_time = dt.datetime.strptime(str(df.Date.values[a]), '%Y-%m-%d')  
            date_time = date_time.replace(hour = int(df.Time[a]))
            df.DateTime.values[a] = date_time
            df.Year.values[a] = date_time.year
        except ValueError:
            df.Year.values[a] = 9999
       
    df_trim = df[ (df.Year >= year_dict[scenario][0]) & (df.Year <= year_dict[scenario][1]) ]
    df_trim = df_trim.reset_index()
    return df_trim.drop(columns=['index'])
    

# output dataframe with ensemble avg of single variable (hourly) with datetime
def fullEnsemble(scenario='HIST'):
    #formatdf = Input(scenario, 'CC', 'all')
    #variables = formatdf.columns
    
    # return dfs -> fullE (so far only have done HIST- check who's missing wind dir
    # ['direction10' in fullE[a].columns for a in fullE.keys()]
    
    dfs = {}
    for Model in model_dict.keys():
        dfs[Model] = Input(scenario, Model, 'all')
    
    dfEns = pd.concat(dfs.values()).groupby(level=0).agg('mean')
    dfEns.index = dfs['CC'].Date
    return dfEns.drop(columns='Year')


# CSV with all variables, date (NOT datetime, for now) as index
def fullCSVoutput(scenario='HIST'):
    Scen = scenario_dict[scenario]
    dirc = '/Users/birke111/Documents/Hourly_CSV/all_models_{}'.format(Scen)
    outpath = os.path.join(dirc, 'ensemble_{}.csv'.format(scenario))
    
    df = fullEnsemble(scenario)
    #df.columns = ['DateTime', 'ensemble_{}'.format(var)]
    df.to_csv(outpath)




# output dataframe with ensemble avg of single variable (hourly) with datetime
def Ensemble(scenario='HIST', var='prcp'):
    df = pd.DataFrame()
    variable = var_dict[var]
    for Model in model_dict.keys():
        df[Model] = Input(scenario, Model, var)[variable].values
        
    df['Ensemble'] = df.mean(axis=1)
    df['DateTime'] = Input(scenario, 'CC', var)['DateTime'].values
    
    #return df
    return df.loc[:, ['DateTime', 'Ensemble']]

# CSV with datetime + single variable
def CSVoutput(scenario='HIST', var='prcp'):
    Scen = scenario_dict[scenario]
    dirc = '/Users/birke111/Documents/Hourly_CSV/all_models_{}'.format(Scen)
    outpath = os.path.join(dirc, 'ensemble_{0}_{1}.csv'.format(scenario, var))
    
    df = Ensemble(scenario, var)
    df.columns = ['DateTime', 'ensemble_{}'.format(var)]
    df.to_csv(outpath)
    

    

