'''
This code calculates a new csv from the csv output of a14lm
(depth-duration frequencies) with a cubic spline smoothing function.
The 8 models' values can be averaged either before or after smoothing.

CSVoutputs have to be created before plotting -
previous plot function performed smoothing independently.
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import interpolate, stats
import os
from a14_lm import PrcpAmts
import probscale

durmin=1
durmax=60
dur_list = [1,2,3,4,7,10,20,30,45,60]

loc_dict = {'ALX':'Alexandria', 'DLH':'Duluth', 'MSP':'Minneapolis', \
            'RST':'Rochester', 'WRN':'Warren' }
scenario_list = ['HIST', 'MID', 'END4.5', 'END8.5']
model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}
    
#dirc = '/Users/birke111/Documents/Atlas14'
# (used this for processing the actual Atlas14 inputs)

# need to restructure (add fromWRF & fullrecord arguments) to avoid having to manually adjust this

#dirc = '/Users/birke111/Documents/Atlas14/AMS_WRF outputs'
#dirc = '/Users/birke111/Documents/Atlas14/AMS_fromHourly outputs'

class Input:
    def __init__(self, dirc, loc, fromWRF, scenario, model, smoothed, fromHourly):

        #CSVinput = os.path.join(dirc, '{} outputs/AMS-based DDF 1990-2009.csv'.format(loc))
        # (used this for processing the actual Atlas14 inputs)
        
        if fromHourly:
            CSVinput = os.path.join(dirc, '{}_DDF.csv'.format(scenario))
        elif smoothed:
            CSVinput = os.path.join(dirc,scenario,loc,'{}_DDF smoothed.csv'.format(model))
        else:
            CSVinput = os.path.join(dirc,scenario,loc,'{}_DDF.csv'.format(model))
        
        #DDF: depth-duration frequency
            #not to be confused with DF as abbrev for DataFrame
        self.DDF = pd.read_csv(CSVinput, index_col=0)

        rows = list(self.DDF.index.values)
        self.durs = rows[ rows.index(durmin) : rows.index(durmax)+1 ]


#def Spline(loc, scenario, model, depths):
    #durs = Input(loc, scenario, model, False).durs
def Spline(depths):
    durs = dur_list
    spline = interpolate.UnivariateSpline(durs, depths)
    return spline(durs)       

#calculates averages across 8 models from either unsmoothed or smoothed DDF files
def modelAvg(loc='MSP', scenario='HIST', smoothedInput=False):
    DDFcomp = np.zeros((8,10,9))
    Models = list(model_dict.keys())
    
    for M in range(len(Models)):
        model_ddf = Input(loc, scenario, Models[M], smoothedInput).DDF
        DDFcomp[M] = model_ddf.values
    DDF_avg = np.mean(DDFcomp, axis=0)
    
    meanDF = pd.DataFrame(data=DDF_avg, index=model_ddf.index, columns=model_ddf.columns)
    return meanDF


'''
This outputs one of the following DDFs:
    1. ModelAvg_DDF.csv, average of 8 unsmoothed models
    2. ModelAvg_DDF smoothed.csv, smoothed after averaging all models (1)
    3. (BC)_DDF smoothed.csv, individual model smoothed
    4. ModelAvg2_DDF smoothed.csv, average of individually smoothed models (3) - possibly less useful than 2??       
'''
def CSVoutput(loc='MSP', scenario='HIST', model='BC', smoothedInput=False, applySmoothing=True):
    #generates a CSV matching Atlas14 output table format (and CSVinput)
    #   (columns = recurrence intervals, rows = durations)
    
    if model=='ModelAvg' or model=='ModelAvg2':     #for 1,2,4
        DDF = modelAvg(loc, scenario, smoothedInput)
    else:                                           #for 3
        DDF = Input(loc, scenario, model, False).DDF
    #durs = Input(loc, scenario, model, False).durs
    durs = dur_list
    
    smoothDDF = pd.DataFrame(data=None, index=durs, columns=DDF.columns)
    for AEP in DDF.columns:
        smoothDDF[AEP] = Spline(DDF.loc[durmin:durmax, str(AEP)].values)
   
    #outpath = os.path.join(dirc, '{} outputs/AMS-based DDF 1990-2009 smoothed.csv'.format(loc))
    #smoothDDF.to_csv(outpath)
    # (used this for processing the actual Atlas14 inputs)
    
    if applySmoothing:     #for 2,3
        outpath = os.path.join(dirc,scenario,loc,'{}_DDF smoothed.csv'.format(model))
        smoothDDF.to_csv(outpath)
    
    else:                   #for 1,4 (4 takes already-smoothed DDFs from 3)
        outpath = os.path.join(dirc,scenario,loc,'{}_DDF.csv'.format(model))
        DDF.to_csv(outpath)
        

#performs all 4 CSV output types in sequence
def CSVoutput_all():
    #1 
    for S in scenario_list:
        for L in loc_dict.keys():
            CSVoutput(L,S,'ModelAvg',smoothedInput=False,applySmoothing=False)
    #2
    for S in scenario_list:
        for L in loc_dict.keys():
            CSVoutput(L,S,'ModelAvg',smoothedInput=False,applySmoothing=True)    
    #3
    for S in scenario_list:
        for L in loc_dict.keys():
            for M in model_dict.keys():
                CSVoutput(L,S,M,smoothedInput=False,applySmoothing=True)    
    #4
    for S in scenario_list:
        for L in loc_dict.keys():
            CSVoutput(L,S,'ModelAvg2',smoothedInput=True,applySmoothing=False) 


'''
Options for plotting:
    Individual model, unsmoothed or smoothed
    Average of models (model='ModelAvg'), smoothed or unsmoothed
    Average of already-smoothed models (model='ModelAvg2', smoothed=False i.e. not smoothed a 2nd time)
'''
def plot(loc='MSP', scenario='HIST', model='ModelAvg', smoothed=False, fromHourly=True):  
    
    DDF = Input(loc, scenario, model, smoothed, fromHourly).DDF
    
    if fromHourly:
        model_str = '8 models averaged hourly'
    elif model=='all':
        model_str = '8-model AMS average'
    elif model=='ModelAvg':
       model_str = '8-model DDF average'
    elif model=='ModelAvg2':
        model_str = 'models averaged post-smoothing'
    else:
       model_str = model_dict[model] 
    
    #durs = Input(loc, scenario, model, False).durs
    durs = dur_list
    
    for AEP in DDF.columns:
        depths = DDF.loc[durmin:durmax, str(AEP)].values
        plt.semilogx(durs, depths)
    
    plt.title('{0} {1}, {2}'.format(loc_dict[loc], scenario, model_str))
    plt.xlabel('Duration (days)')
    plt.ylabel('Precipitation depth (in)')
    legendstr = 'Annual exceedance \nprobability (1/years)'
    plt.legend(DDF.columns.values, title=legendstr, loc=0, bbox_to_anchor=(1,0.9))
    plt.xticks(ticks=durs, labels=durs)
    plt.xlim(durmin, durmax)
    #plt.ylim(-1,29)
    plt.grid(linestyle='--')



# use for now, can remove later when accessing from sample_run
# will need to add inputs_path argument to this
inputs_path_obsv = '/Users/birke111/Documents/Atlas14/inputs/'
inputs_path_wrf = '/Users/birke111/Documents/Climate Data/derived_stats'
    
class plotGEV:
        
    def __init__(self, loc='MSP', duration=1, fromWRF=True, scenario='HIST', model='all', fullrecord=True, init=1837,
             end=2010, multiScenario=False, scenario2='MID', fromHourly=False, multiModel=False, includeLast=True):
        
        if fromWRF:
            in_path = inputs_path_wrf
        else:
            in_path = inputs_path_obsv
                
        P = PrcpAmts(in_path, loc, duration, fromWRF, scenario, model, fullrecord, init, end, multiScenario, scenario2, fromHourly, multiModel)
        self.AMS = P.AMS
        
        if includeLast:     # otherwise the highest value (100th percentile) is not displayed
            shift = (100 / len(self.AMS)) / 2    # shifts each point from right to center of "bin"
        else:
            shift = 0
        self.AMSpercentiles = [100 - stats.percentileofscore(self.AMS, i) + shift for i in self.AMS]
        
        if multiModel:
            self.multiAMS = P.multiAMS
            multiAMSpct = [100 - stats.percentileofscore(self.multiAMS.flatten(), i) \
                                   + shift for i in self.multiAMS.flatten()]
            self.multiAMSpercentiles = np.array(multiAMSpct).reshape((8,20))
            
        
        x = np.linspace(0.001, 0.999, 999)
        self.X = 100*(1-x)
        self.Y = P.fitted_gev.ppf(x)

           
        if fromWRF:
            self.titlestr = scenario + ' 8-model AMS average'
        elif fullrecord:
            self.titlestr = 'Full observational record'
        else:
            self.titlestr = '{0}-{1} observational record'.format(init, end)
             
        
'''
By default: groups GEV curves by scenario (4 panels, skips either observational or END4.5)
    Any number of durations works - enter them in "durations" argument brackets
    
Otherwise (groupbyDurations=True): self explanatory, but need to enter 4 durations (1 per panel)
    Disregards includeObsv function (for now) - observational + 4 model scenarios all included
'''    
def multiplotGEV(loc='MSP', durations=[1,10,30], includeObsv=True, groupbyDurations=False, multiModel=False):
    
    gev_obsv = [plotGEV(loc, dur, fromWRF=False, fullrecord=True, includeLast=True) for dur in durations]
    gev_hist = [plotGEV(loc, dur, fromWRF=True, scenario='HIST', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    gev_mid  = [plotGEV(loc, dur, fromWRF=True, scenario='MID', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    gev_end4 = [plotGEV(loc, dur, fromWRF=True, scenario='END4.5', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    gev_end8 = [plotGEV(loc, dur, fromWRF=True, scenario='END8.5', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    
    if groupbyDurations:
        plots = [[gev_obsv[i], gev_hist[i], gev_mid[i], gev_end4[i], gev_end8[i]] for i in range(len(durations))]
        legendGroup = ['Full record', 'HIST', 'MID', 'END4.5', 'END8.5']
        legendTitle = 'Scenarios'
    
    else:
        if includeObsv:
            plots = [gev_obsv, gev_hist, gev_mid, gev_end8]
        else:
            plots = [gev_hist, gev_mid, gev_end4, gev_end8]  
        legendGroup = durations
        legendTitle = 'Duration (days)'
        
    probs = [0.1, 0.5, 1, 2, 4, 10, 20, 50]  # 0.2 corresponds to 500y recurrence, omitted to declutter labels
        
    fig, axes = plt.subplots(2, 2, figsize=(13,10))
    fig.subplots_adjust(hspace=0.3)
    
    for ax, a in zip(axes.flatten(), range(len(plots))):
        subplot = plots[a]
        for i in range(len(legendGroup)):
            ax.plot(subplot[i].X, subplot[i].Y)
            ax.scatter(subplot[i].AMSpercentiles, subplot[i].AMS)
        
        ax.set_xlim(left=99.9, right=0.1)
        ax.set_xscale('prob')
        ax.set_xticks(probs)
        ax.set_xlabel('Annual exceedance probability (%)')
        ax.set_ylabel('Precipitation (in)')
        ax.grid(True)
        if groupbyDurations:
            ax.set_title(str(durations[a]) + '-day duration')
        else:    
            ax.set_title(subplot[i].titlestr)
    
    fig.suptitle('Generalized Extreme Value Distributions ({})'.format(loc_dict[loc]), fontsize=18)
    fig.legend(labels=legendGroup, title=legendTitle, bbox_to_anchor=(1.02, 0.55))
    
    
# plotting one duration at a time, multiModel with each model color coded
# incorporate later into original multiplotGEV, just trying to avoid messing any of that up for now!
# 4 subplots, 4 durations, each containing a single curve for 1 scenario

def multiplotGEV2(loc='MSP', scenario='HIST', durations=[1,10,30,60], includeObsv=True, multiModel=True, wide=True):
    
    gev_obsv = [plotGEV(loc, dur, fromWRF=False, fullrecord=True, includeLast=True) for dur in durations]
    gev_hist = [plotGEV(loc, dur, fromWRF=True, scenario='HIST', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    gev_mid  = [plotGEV(loc, dur, fromWRF=True, scenario='MID', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    gev_end4 = [plotGEV(loc, dur, fromWRF=True, scenario='END4.5', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    gev_end8 = [plotGEV(loc, dur, fromWRF=True, scenario='END8.5', model='all', multiModel=multiModel, includeLast=True) for dur in durations]
    
    scen_dict = {'HIST':gev_hist, 'MID':gev_mid, 'END4.5':gev_end4, 'END8.5':gev_end8}
    
    #plots = [scen_dict[scenario][i] for i in range(len(durations))]
    plots = scen_dict[scenario]
    legendGroup = list(model_dict.values())
    legendTitle = 'Models'
 
        
    probs = [0.1, 0.5, 1, 2, 4, 10, 20, 50, 80, 90, 99, 99.9]  # 0.2 corresponds to 500y recurrence, omitted to declutter labels
    
    if wide:
        width = 19
        legend_bbox = (1.3, 1.2)
    else:
        width = 13
        legend_bbox = (1.4, 1.2)
    
    fig, axes = plt.subplots(2, 2, figsize=(width,10)) # normal width=13, wider=19
        
    fig.subplots_adjust(hspace=0.3)
    
    for ax, a in zip(axes.flatten(), range(len(plots))):
        subplot = plots[a]
        #ax.plot(subplot.X, subplot.Y)
        
        for i in range(len(legendGroup)):
            ax.scatter(subplot.multiAMSpercentiles[i], subplot.multiAMS[i],
                       label=legendGroup[i])
        
        ax.set_xlim(left=99.9, right=0.1)
        ax.set_xscale('prob')
        ax.set_xticks(probs)
        ax.set_xlabel('Annual exceedance probability (%)')
        ax.set_ylabel('Precipitation (in)')
        ax.grid(True)
        ax.set_title(str(durations[a]) + '-day duration')
        
    
    fig.suptitle('Generalized Extreme Value Distributions ({0}, {1})'.format(loc_dict[loc], scenario), fontsize=18)
    #fig.legend(labels=legendGroup, title=legendTitle, bbox_to_anchor=(1.02, 0.55))
    plt.legend(title=legendTitle, loc='center right', bbox_to_anchor=legend_bbox)








# single plot, superimpose same-duration curves - include multiple durations    
# this gets cluttered, so introduced groupbyDurations argument to multiplotGEV instead
def plotGEV_bydur(loc='MSP', durations=[1,30]):
    
    scenarios = [[plotGEV(loc='MSP', duration=dur, fromWRF=False, fullrecord=True, includeLast=True) for dur in durations]]
    for S in scenario_list:
        scenarios.append([plotGEV(loc='MSP', duration=dur, fromWRF=True, scenario=S, model='all', includeLast=True) for dur in durations])
    
    # scenarios shape is now (5,2)
    
    probs = [0.1, 0.5, 1, 2, 4, 10, 20, 50]
    # 0.2 corresponds to 500y recurrence, omitted here to declutter axis labels
        
    fig, ax = plt.subplots(1, 1, figsize=(10,8))
    #fig.subplots_adjust(hspace=0.3)
    
    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple']
    markers = ['o', 'P', '^', 's', 'D']     # likely use 2-3, this way allows up to 5
    

    for i in range(np.shape(scenarios)[0]):         # scenarios (5)
        for j in range(np.shape(scenarios)[1]):     # durations (2-3?)
            ax.plot(scenarios[i][j].X, scenarios[i][j].Y, c=colors[i])
            ax.scatter(scenarios[i][j].AMSpercentiles, scenarios[i][j].AMS, c=colors[i], marker=markers[j])
           
    ax.set_xlim(left=99.9, right=0.1)
    ax.set_xscale('prob')
    ax.set_xticks(probs)
    ax.set_xlabel('Annual exceedance probability (%)')
    ax.set_ylabel('Precipitation (in)')
    #ax.set_title(scenarios[i].titlestr)
    ax.grid(True)
    
    #fig.legend(labels=['Full record', 'HIST', 'MID', 'END4.5', 'END8.5'], \
    #           title='Scenarios', bbox_to_anchor=(1.05, 0.55))
    
    
        