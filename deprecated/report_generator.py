from pathlib import Path
import os
import pandas as pd
import AMS_to_DDF as DDF
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


loc_list = ['Douglas']
# loc_list = ['Austin City MS4']
inputs_path_wrf = 'C:/Users/rrnoe/Work/Stormwater/v2/county_ams'
# inputs_path_wrf = r'C:\Users\rrnoe\Work\Stormwater\v2\Austin\Tabular Output'
applySmoothing = True


def make_ddf_figure(ax, df, location, scenario, durmin=1, durmax=60):
    rows = list(df.index.values)
    durs = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

    for AEP in df.columns:
        depths = df.loc[durmin:durmax, AEP].values
        ax.semilogx(durs, depths)

    plt.title('{0} County ({1})'.format(location, scenario))
    plt.xlabel('Duration (days)')
    plt.ylabel('Precipitation depth (in)')
    legendstr = 'Annual \nexceedance \nprobability \n(1/years)'
    plt.legend(df.columns.values, title=legendstr, loc=0, bbox_to_anchor=(1, 0.9))
    plt.xticks(ticks=durs, labels=durs)
    plt.xlim(durmin, durmax)
    plt.grid(linestyle='--')


def make_pf_table(ax, df, edge_color='grey', edge_width=0.5):
    columns = ('2', '5', '10', '25', '50', '100')
    row_headers = [' 1 ', ' 2 ', ' 3 ', ' 4 ', ' 7 ', ' 10 ', ' 20 ', ' 30 ', ' 45 ', ' 60 ']

    rcolors = ['#CCD9C7'] * len(row_headers)
    ccolors = ['#CCD9C7'] * len(columns)

    pf_table = ax.table(cellText=df.values, rowLabels=row_headers, rowColours=rcolors, rowLoc='center',
                        colLabels=columns, colColours=ccolors, colWidths=[0.1666] * 6, loc=9)
    ax.yaxis.set_label_coords(-0.055, 0.5)
    # pf_table.auto_set_font_size(False)
    # pf_table.set_fontsize(9)

    ax.set_frame_on(False)
    ax.set_title('Precipitation Depth Estimates - inches')
    ax.set_xlabel('Average recurrence interval - years')
    ax.set_ylabel('Duration - days')
    ax.xaxis.set_label_position('top')
    ax.axes.xaxis.set_ticks([])
    ax.axes.yaxis.set_ticks([])

    for key, cell in pf_table.get_celld().items():
        cell.set_edgecolor(edge_color)
        cell.set_linewidth(edge_width)
        # cell.set_fontsize(24)
        cell.set_height(1/(len(df)+1))   # 1 / number of rows in table plus the header


def colormap(val):
    if val >= 60:
        return '#3182bd'
    elif 30 <= val < 60:
        return '#9ecae1'
    elif 15 <= val < 30:
        return '#deebf7'
    elif 15 > val > -15:
        return 'white'
    elif -15 >= val > -30:
        return '#fee6ce'
    elif -30 >= val > -60:
        return '#fdae6b'
    elif val <= -60:
        return '#e6550d'


def make_color_pf_table(ax, df, condf, edge_color='grey', edge_width=0.5):
    cl_outcomes = {
        'white': '#FFFFFF',
        'gray': '#AAA9AD',
        'black': '#313639',
        'purple': '#AD688E',
        'orange': '#D18F77',
        'yellow': '#E8E190',
        'ltgreen': '#CCD9C7',
        'dkgreen': '#96ABA0',
    }
    columns = ('2', '5', '10', '25', '50', '100')
    row_headers = [' 1 ', ' 2 ', ' 3 ', ' 4 ', ' 7 ', ' 10 ', ' 20 ', ' 30 ', ' 45 ', ' 60 ']
    rcolors = ['#CCD9C7'] * len(row_headers)
    ccolors = ['#CCD9C7'] * len(columns)

    colors = condf.applymap(lambda x: colormap(x))

    pf_table = ax.table(cellText=df.values, rowLabels=row_headers, rowColours=rcolors, rowLoc='center',
                        colLabels=columns, colColours=ccolors, colWidths=[0.1666] * 6, cellColours=colors.to_numpy(), loc=9)
    ax.yaxis.set_label_coords(-0.055, 0.5)
    # pf_table.auto_set_font_size(False)
    # pf_table.set_fontsize(9)

    ax.set_frame_on(False)
    ax.set_title('Precipitation Depth Estimates - inches')
    ax.set_xlabel('Average recurrence interval - years')
    ax.set_ylabel('Duration - days')
    ax.xaxis.set_label_position('top')
    ax.axes.xaxis.set_ticks([])
    ax.axes.yaxis.set_ticks([])

    for key, cell in pf_table.get_celld().items():
        cell.set_edgecolor(edge_color)
        cell.set_linewidth(edge_width)
        # cell.set_fontsize(24)
        cell.set_height(1/(len(df)+1))   # 1 / number of rows in table plus the header


def make_combined_pf_df(df, pct_df):
    combined = pd.DataFrame()
    years = [2, 5, 10, 25, 50, 100]
    for year in years:
        combined[year] = pct_df[year].map(str)+'% ' + '('+df[year].map(str).str.ljust(4, fillchar='0')+')'
    return combined


def make_combined_pf_df2(df, abs_df, pct_df):
    combined = pd.DataFrame()
    years = [2, 5, 10, 25, 50, 100]
    for year in years:
        combined[year] = df[year].map(str) + '\n(' + abs_df[year].map(str) + ')' + '\n' + pct_df[year].map(str) + '%'
    return combined


def reshape_noaa_table(df, location):
    years = [2, 5, 10, 25, 50, 100]
    durs = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]

    df = df.loc[df['COUNTYNAME'] == location].reset_index()
    # df = df.loc[df['MS4_Name'] == location].reset_index()
    rows = []

    for dur in durs:
        row = []
        for year in years:
            colname = f'a14_{dur:02}d_{year}yr'
            val = df.at[0, colname]
            row.append(val)
        rows.append(row)

    reshaped = pd.DataFrame(rows, columns=years)
    reshaped.index = durs
    return reshaped


noaa_all = pd.read_csv(r"C:\Users\rrnoe\Work\Stormwater\v2\noaa_grids\noaa_county_avg.csv")
# noaa_all = pd.read_csv(r"C:\Users\rrnoe\Work\Stormwater\v2\Austin\noaa_Austin_avg.csv")
n = 0

for loc in loc_list:
    noaa = reshape_noaa_table(noaa_all, loc)
    noaa = noaa.round(decimals=2)

    hist = DDF.output_forCSV(inputs_path_wrf, loc=loc, scenario='HIST', applySmoothing=True, n_row_drop=n)
    hist_absdif_noaa = (hist - noaa).round(decimals=2)
    hist_pctdif_noaa = (((hist - noaa) / noaa) * 100).round(decimals=1)
    hist = hist.round(decimals=2)
    hist_page = [hist, 'na', 'na', hist_absdif_noaa, hist_pctdif_noaa, 'HIST 1980-1999']

    # mid = DDF.output_forCSV(inputs_path_wrf, loc=loc, scenario='MID', applySmoothing=True, n_row_drop=n)
    # mid_absdif_hist = (mid - hist).round(decimals=2)
    # mid_pctdif_hist = (((mid - hist) / hist) * 100).round(decimals=1)
    # mid_absdif_noaa = (mid - noaa).round(decimals=2)
    # mid_pctdif_noaa = (((mid - noaa) / noaa) * 100).round(decimals=1)
    # mid = mid.round(decimals=2)
    # mid_page = [mid, mid_absdif_hist, mid_pctdif_hist, mid_absdif_noaa, mid_pctdif_noaa, '2040-2059']

    # end45 = DDF.output_forCSV(inputs_path_wrf, loc=loc, scenario='END4.5', applySmoothing=True, n_row_drop=n)
    # end45_absdif_hist = (end45 - hist).round(decimals=2)
    # end45_pctdif_hist = (((end45 - hist) / hist) * 100).round(decimals=1)
    # end45_absdif_noaa = (end45 - noaa).round(decimals=2)
    # end45_pctdif_noaa = (((end45 - noaa) / noaa) * 100).round(decimals=1)
    # end45 = end45.round(decimals=2)
    # end45_page = [end45, end45_absdif_hist, end45_pctdif_hist, end45_absdif_noaa, end45_pctdif_noaa, '2080-2099 RCP4.5']

    end85 = DDF.output_forCSV(inputs_path_wrf, loc=loc, scenario='END8.5', applySmoothing=True, n_row_drop=n)
    end85_absdif_hist = (end85 - hist).round(decimals=2)
    end85_pctdif_hist = (((end85 - hist) / hist) * 100).round(decimals=1)
    end85_absdif_noaa = (end85 - noaa).round(decimals=2)
    end85_pctdif_noaa = (((end85 - noaa) / noaa) * 100).round(decimals=1)
    end85 = end85.round(decimals=2)
    end85_page = [end85, end85_absdif_hist, end85_pctdif_hist, end85_absdif_noaa, end85_pctdif_noaa, '2080-2099 RCP8.5']

    # midend45 = DDF.output_forCSV(inputs_path_wrf, loc=loc, multiscen=['MID', 'END4.5'], applySmoothing=True, n_row_drop=n)
    # midend45_absdif_hist = (midend45 - hist).round(decimals=2)
    # midend45_pctdif_hist = (((midend45 - hist) / hist) * 100).round(decimals=1)
    # midend45_absdif_noaa = (midend45 - noaa).round(decimals=2)
    # midend45_pctdif_noaa = (((midend45 - noaa) / noaa) * 100).round(decimals=1)
    # midend45 = midend45.round(decimals=2)
    # midend45_page = [midend45, midend45_absdif_hist, midend45_pctdif_hist, midend45_absdif_noaa, midend45_pctdif_noaa, '2040-59 + 2080-99 RCP4.5']

    # midend85 = DDF.output_forCSV(inputs_path_wrf, loc=loc, multiscen=['MID', 'END8.5'], applySmoothing=True, n_row_drop=n)
    # midend85_absdif_hist = (midend85 - hist).round(decimals=2)
    # midend85_pctdif_hist = (((midend85 - hist) / hist) * 100).round(decimals=1)
    # midend85_absdif_noaa = (midend85 - noaa).round(decimals=2)
    # midend85_pctdif_noaa = (((midend85 - noaa) / noaa) * 100).round(decimals=1)
    # midend85 = midend85.round(decimals=2)
    # midend85_page = [midend85, midend85_absdif_hist, midend85_pctdif_hist, midend85_absdif_noaa, midend85_pctdif_noaa, '2040-59 + 2080-99 RCP8.5']


    # pages = [mid_page, end45_page, end85_page, midend45_page, midend85_page]
    # table_pages = [hist_page, mid_page, end45_page, end85_page, midend45_page, midend85_page]
    pages = [end85_page]
    table_pages = [hist_page]

    with PdfPages(f'c:/users/rrnoe/desktop/{loc}_{20-n}yr.pdf') as pdf:
        ########################################### Page 1 ###################################################
        for page in pages:
            fig = plt.figure(figsize=(8.5, 11), dpi=300)

            ax1 = fig.add_subplot(211)
            make_ddf_figure(ax1, page[0], loc, page[5])
            cb = ax1.get_position()
            ax1.set_position([cb.x0, 0.66, cb.width * 0.85, cb.height * 0.72])

            t2 = ("Projected precipitation depths for 2080-2099 under high emissions scenario (RCP 8.5). These curves were\n" 
                 "created from climate change projections using methods consistent with those used to create NOAA Atlas-14.\n"
                 "However, these estimates should be interpreted carefully because they are based on a 20-year annual\n"
                 "maximum series, while NOAA estimates typically use observations from at least 30 years, and sometimes\n"
                 "more than 100. Despite a limited series, our modeled 1980-1999 data showed favorable agreement with\n"
                 "NOAA estimates for many durations, frequencies, and locations. See tables below and summary report for\n"
                 "strategies for interpretingthese estimates that consider the limitations of the underlying data.")


            # plt.text(-0.04, -0.05, t2, fontsize=9.5, va='bottom', ha='left')
            plt.text(0.7, -5.5, t2, fontsize=9.5, va='top', ha='left')

            ax2 = fig.add_subplot(212)
            combined_df = make_combined_pf_df(page[0], page[2])

            make_pf_table(ax2, combined_df)
            cb = ax2.get_position()
            ax2.set_position([cb.x0, 0.2, cb.width, cb.height * 0.72])

            t = ("Percent change between our modeled 1980-1999 and 2080-2099 estimates, with precipitation depth for\n"
                 "2080-2099 estimates shown in parentheses. The preferred way to interpret climate change projections\n"
                 "is to compare a modeled historical scenario with a future scenario because the model assumptions that\n"
                 "led to under or over-estimates in the historical period will be consistent in the future scenario. This is\n"
                 "important when interpreting our short durations or long recurrence intervals (e.g., 1 day or 100 years),\n"
                 "as these often had the greatest difference between our modeled historical data and NOAA estimates.")

            plt.text(-0.076, -0.05, t, fontsize=9.5, va='top', ha='left')
            # plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)

            pdf.savefig()
            plt.close()

        for page in table_pages:
            fig = plt.figure(figsize=(8.5, 11), dpi=300)

            ax2 = fig.add_subplot(111)
            combined_df = make_combined_pf_df2(page[0], page[3], page[4])
            make_color_pf_table(ax2, combined_df, page[4])
            cb = ax2.get_position()
            ax2.set_position([cb.x0, 0.3, cb.width, cb.height * 0.72])


            t = ("Differences between our modeled historical data and NOAA estimates. The first line in each cell is the\n"
                 "precipitation depth estimate based on modeled 1980-1999 data. The second line is the difference in\n"
                 "inches between our estimate and NOAA. The third line is the percent difference between the estimates.\n"
                 "Differences between -15% and 15% are shaded white, 15% to 30% light blue, 30% to 60% blue,\n"
                 "and >60% dark blue. The shading intervals are reversed using shades of orange for negative\n"
                 "values. These values indicate how well our data and methods were able to re-create NOAA estimates\n"
                 "using a much shorter time series (1980-1999) of modeled data. However, because our modeled data \n"
                 "represents the end of the 20th century, some differences between the estimates may be due to climate\n"
                 "change. Users that intend to use modeled precipitation depths rather than relative differences should\n"
                 "consult this table to check how well our projections performed for the county, duration, and recurrence\n"
                 "interval of interest. These values can also be used to adjust projected values.")

            plt.text(-0.04, -0.05, t, fontsize=9.5, va='top', ha='left', wrap=True)


            pdf.savefig()
            plt.close()

    # with PdfPages('c:/users/rrnoe/desktop/multipage_pdf.pdf') as pdf:
    #     ########################################### Page 1 ###################################################
    #     fig = plt.figure(figsize=(8.5, 11), dpi=300)
    #
    #     ax1 = fig.add_subplot(211)
    #     make_ddf_figure(ax1, mid, loc, '2040-2059')
    #     cb = ax1.get_position()
    #     ax1.set_position([cb.x0, 0.63, cb.width * 0.85, cb.height * 0.72])
    #
    #     ax2 = fig.add_subplot(212)
    #     combined_df = make_combined_pf_df(mid, mid_pctdif_hist)
    #     make_pf_table(ax2, combined_df)
    #     cb = ax2.get_position()
    #     ax2.set_position([cb.x0, 0.2, cb.width, cb.height * 0.72])
    #
    #
    #     t = ("Values in cells above represent the precipitation depth in inches for a given duration and recurrence\n"
    #          "interval in the county. Values in parentheses represent the percent change between our modeled\n"
    #          "1980-1999 data our modeled scenario data. In general, the relative differences between two sets\n"
    #          "of modeled data is the preferred way to interpret projections related to climate change.")
    #
    #     plt.text(-0.04, -0.05, t, fontsize=9.5, va='top', ha='left')
    #     # plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.1)
    #
    #     pdf.savefig()
    #     plt.close()
    #
    #     ########################################### Page 2 ###################################################
    #     fig2 = plt.figure(figsize=(8.5, 11), dpi=300)
    #
    #     ax3 = fig2.add_subplot(211)
    #     make_ddf_figure(ax3, end85, loc, '2080-2099')
    #     cb = ax3.get_position()
    #     ax3.set_position([cb.x0, 0.63, cb.width * 0.85, cb.height * 0.72])
    #
    #     ax4 = fig2.add_subplot(212)
    #     combined_df = make_combined_pf_df(end85, end85_pctdif_hist)
    #     make_pf_table(ax4, combined_df)
    #     cb = ax4.get_position()
    #     ax4.set_position([cb.x0, 0.2, cb.width, cb.height * 0.72])
    #
    #     plt.text(-0.04, -0.05, t, fontsize=9.5, va='top', ha='left')
    #
    #     pdf.savefig()
    #     plt.close()
    #
    #     ########################################### Page 3 ###################################################
    #     fig3 = plt.figure(figsize=(8.5, 11), dpi=300)
    #
    #     ax5 = fig3.add_subplot(411)
    #     # combined_df = make_combined_pf_df(end85_df, end85_pct_chg)
    #     make_pf_table(ax5, noaa)
    #     cb = ax5.get_position()
    #     ax5.set_position([cb.x0, cb.y0, cb.width, cb.height * 0.72])
    #
    #     ax6 = fig3.add_subplot(412)
    #     # combined_df = make_combined_pf_df(end85_df, end85_pct_chg)
    #     make_pf_table(ax6, end85)
    #     cb = ax6.get_position()
    #     ax6.set_position([cb.x0, cb.y0, cb.width, cb.height * 0.72])
    #
    #     ax7 = fig3.add_subplot(413)
    #     # combined_df = make_combined_pf_df(end85_df, end85_pct_chg)
    #     make_pf_table(ax7, end85_absdif_noaa)
    #     cb = ax7.get_position()
    #     ax7.set_position([cb.x0, cb.y0, cb.width, cb.height * 0.72])
    #
    #     ax8 = fig3.add_subplot(414)
    #     # combined_df = make_combined_pf_df(end85_df, end85_pct_chg)
    #     make_pf_table(ax8, end85_pctdif_noaa)
    #     cb = ax8.get_position()
    #     ax8.set_position([cb.x0, cb.y0, cb.width, cb.height * 0.72])
    #
    #
    #     pdf.savefig()
    #     plt.show()
    #     plt.close()
