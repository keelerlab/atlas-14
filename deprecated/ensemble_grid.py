'''
Reads in netcdf grids - 8 models, daily
Output for now: daily gridcell averages for precip at least, if not full set of variables
    Variables: time/lat/lon/level, tmin/tmax/prec/relh/wspd/rads
    Version for repo: match original format

Later: output netcdfs with other processing
    AMS averages for each gridcell
    
'''

import os
import numpy as np
import pandas as pd
import netCDF4 as nc
import datetime as dt
from pathlib import Path

scenario_dict = {'HIST':'HISTORIC/1980-1999', 'MID':'RCP4.5/2040-2059', \
                 'END4.5':'RCP4.5/2080-2099', 'END8.5':'RCP8.5/2080-2099'}
scenario_list = list(scenario_dict.keys())
 
#later: fix this redundancy with above
year_dict = {'HIST':[1980,1999], 'MID':[2040,2059], \
             'END4.5':[2080,2099], 'END8.5':[2080,2099]}

model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}
    
    
inputs_path = '/Users/birke111/Documents/LCCMR_IBIS'
base_outpath = '/Users/birke111/Documents/nctest'


class ModelGrid:
    
    def __init__(self, in_path, model, scenario):
        self.in_path = in_path
        self.model = model
        self.scenario = scenario
        self.yrInit, self.yrEnd = year_dict[scenario][0:2] 
        self.lats, self.lons = self.getLatLon()
        
        
    def Input(self, year): # can input year=0 through 19 (yrEnd - yrInit)
        Year = self.yrInit + year        
        ncfile = os.path.join(self.in_path, 
                              model_dict[self.model], 
                              scenario_dict[self.scenario],
                              'WRF_IBISinput/IBISinput_{}.nc'.format(Year))
        return nc.Dataset(ncfile)
        
    def getLatLon(self):
        ds = self.Input(year=0) # shouldn't matter which year, but need to enter one
        lats = ds.variables['lat'][:]
        lons = ds.variables['lon'][:]
        return lats, lons
    
    def singleYear(self, year, variable):
        ds = self.Input(year)
        var = ds.variables[variable]
                
        dailyVar = [var[day, 0, :, :] for day in range(len(var))]
        
        #Year = self.yrInit + year
        #dates = [dt.datetime(Year,1,1) + dt.timedelta(days=day) for day in range(len(var))]

        return dailyVar
    
        
def gridEnsemble(scenario='HIST', year=0, variable='prec'):
    grids = []
    for model in model_dict.keys():
        grids.append(ModelGrid(model, scenario).singleYear(year, variable))
    
    ensGrid =  np.mean(np.array(grids), axis=0)
    ensGrid[ensGrid > 1e+3] = np.nan    
        # removes fill values in cells without data (Canada, Lake Superior, edges)
    return ensGrid    


def ensembleNC(scenario='HIST', year=0, variable='prec'):
    
    modelgrid = ModelGrid('BC', scenario) # BC is arbitrary but need one
    Year = modelgrid.yrInit + year
    
    ensembleVar = gridEnsemble(scenario, year, variable)
    
    outpath = os.path.join(base_outpath, scenario, 'ensemble')
    Path(outpath).mkdir(parents=True, exist_ok=True)
    
    outfile = os.path.join(outpath, '{0}_{1}.nc'.format(variable, Year))
    ds = nc.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')
    
    time = ds.createDimension('time', len(ensembleVar))
    lat = ds.createDimension('lat', len(modelgrid.lats))
    lon = ds.createDimension('lon', len(modelgrid.lons))
    lev = ds.createDimension('lev', 1)
    
    times = ds.createVariable('time', 'f4', ('time',))
    times.units = 'days since {}-12-31'.format(Year - 1)
        
    lats = ds.createVariable('lat', 'f4', ('lat',))
    lats.units = 'degrees_north'
    lons = ds.createVariable('lon', 'f4', ('lon',))
    lons.units = 'degrees_west'
    levs = ds.createVariable('lev', 'f4', ('lev',))
    var = ds.createVariable(variable, 'f4', ('time', 'lev', 'lat', 'lon',)) 
    # original files have fill_value=9e+20 - not assigning correctly yet here but may not matter?
    # later: add multiple variables (maybe all from the original netcdfs)
       
    lats[:] = modelgrid.lats[:]
    lons[:] = modelgrid.lons[:]
    
    julianDays = np.arange(len(ensembleVar)) + 1
    times[:] = julianDays
    var[:] = ensembleVar
    

def createNC_fullset(variable='prec'):
    for scenario in scenario_list:
        for year in range(20):
            ensembleNC(scenario, year, variable)
            
    
        