'''
This code calculates depth-duration frequencies from annual maximum series.
L-moment calculations from each duration (1d to 60d) are aggregrated for a 
single location/scenario/model.

If using historical observation AMS, can calculate from either the full period
of record or a designated subset of it.
'''


import numpy as np
import pandas as pd
from lmoments3a import distr
import os

# provides period of record for each location (most have years missing)
# end values are inclusive, as used in df.loc function
period_dict = {'ALX': [1889, 2010], 'DLH': [1941, 2012], 'MSP': [1837, 2010], 'RST': [1893, 2010], 'WRN': [1903, 1975]}

loc_dict = {'ALX': 'Alexandria', 'DLH': 'Duluth', 'MSP': 'Minneapolis', 'RST': 'Rochester', 'WRN': 'Warren'}

# Average recurrence intervals
ARI_list = np.array([2, 5, 10, 25, 50, 100, 200, 500, 1000])

# Durations used in Atlas14, in days (1d and above)
dur_list = [1, 2, 3, 4, 7, 10, 20, 30, 45, 60]
durlabels = [str(a) + 'd' for a in dur_list]

scenario_dict = {'HIST', 'MID', 'END4.5', 'END8.5'}
model_dict = {'BC': 'bcc-csm1-1', 'CC': 'CCSM4', 'CM': 'CMCC-CM',
              'CN': 'CNRM-CM5', 'GF': 'GFDL-ESM2M', 'IP': 'IPSL-CM5A-LR',
              'MR': 'MRI-CGCM3', 'MI': 'MIROC5'}


# Annual exceedance probability (subtracted from 1 for PPF)
def AEP(ARI):
    aep = 1 / ARI
    return 1 - aep


# AEP_list = AEP(ARI_list)

# returns constrained AMS for downloaded Atlas 14 inputs
def AMS_obsv(dirc, loc='MSP', duration=1, fullrecord=True, init=1837, end=2010):
    # AMSfile = '/Users/birke111/Documents/Atlas14/inputs/{}_AMS.csv'.format(loc)
    AMSfile = os.path.join(dirc, '{}_AMS.csv'.format(loc))

    AMSdf = pd.read_csv(AMSfile, index_col=0)

    if fullrecord:
        init = period_dict[loc][0]
        end = period_dict[loc][1]
    
    # Still usable but no longer necessary - 
    #  code no longer produces an error if selected range is wider than actual record.
    '''
    if init not in AMSdf.index:
        print('No data for initial year of selected record in {}'.format(loc))
    if end not in AMSdf.index:
        print('No data for ending year of selected record in {}'.format(loc))
    '''
    
    # Include this instead of the above to display ALL years without data if desired.
    # Could remove "else" if that's useful to know even when setting fullrecord=True.
    # However, printed message is repeatedly displayed if looping from sample_run.
    '''
    else: 
        nodata_years = [year for year in range(init,end) if year not in AMSdf.index]
        print('No data for years of selected record in {}:'.format(loc), nodata_years)
    '''

    AMSconstrained = AMSdf.loc[init:end, '{}d'.format(duration)].values
    AMSconstrained = AMSconstrained[~np.isnan(AMSconstrained)]

    return AMSconstrained


# returns constrained AMS for model output-based AMS files
def AMS_WRF(dirc, loc='MSP', duration=1, scenario='HIST', model=''):
    if duration == 1:
        AMSfile = os.path.join(dirc, '{}_year_max_prec_ams.csv'.format(scenario))
    else:
        AMSfile = os.path.join(dirc, '{0}_year_max_consum_prec_{1}_days_ams.csv'.format(scenario, duration))
    AMSdf = pd.read_csv(AMSfile, index_col=0)

    if model == 'all':
        indivModelDF = AMSdf.groupby('year').agg('mean')
    else:
        indivModelDF = AMSdf[AMSdf.model == model_dict[model]]   

    AMSmm = indivModelDF.loc[:, loc_dict[loc]].values
    AMSinches = AMSmm / 25.4
    return AMSinches

# string together AMS_WRF for 2 scenarios, create a 40-year record
def MultiScenario(dirc, loc='MSP', duration=1, scenario1='HIST', scenario2='MID', model=''):
    AMS1 = AMS_WRF(dirc, loc, duration, scenario1, model)
    AMS2 = AMS_WRF(dirc, loc, duration, scenario2, model)
    
    return np.append(AMS1, AMS2)
   

# Calculates L-moments and returns precip depths for a given duration
def PrcpAmts(inputs_path, loc='MSP', duration=1, fromWRF=True, scenario='HIST', model='', fullrecord=True, init=1837,
             end=2010, multiScenario=False, scenario2='MID'):
    # Correction factors vary by duration (from Table 4.5.1)
    # this converts constrained to unconstrained values
    CFdict = {1: 1.12, 2: 1.04, 3: 1.03, 4: 1.02, 7: 1.01, '>7': 1}
    Dur = duration
    if Dur > 7:
        Dur = '>7'
    corr_factor = CFdict[Dur]

    if fromWRF:
        if multiScenario:
            AMSconstrained = MultiScenario(inputs_path, loc, duration, scenario, scenario2, model)
        else:
            AMSconstrained = AMS_WRF(inputs_path, loc, duration, scenario, model)
    else:
        AMSconstrained = AMS_obsv(inputs_path, loc, duration, fullrecord, init, end)
    AMSunconstrained = np.multiply(AMSconstrained, corr_factor)

    paras = distr.gev.lmom_fit(AMSunconstrained)
    fitted_gev = distr.gev(**paras)

    precip = fitted_gev.ppf(AEP(ARI_list))
    return precip


# Calculates L-moments from hourly ensemble outputs (all durations already combined)
# Skips correction factor since durations aren't constrained by date (12am-12am sums)
#       (need to confirm: is that how the daily netcdfs aggregated precip values?)

def PrcpAmts_fromHourly(inputs_path, scenario='HIST', duration=1):
    # pull single dur from df, recombine later for csvoutput
    Scen_dict = {'HIST':'historical', 'MID':'RCP4.5', 'END4.5':'RCP4.5', 'END8.5':'RCP8.5'}
    AMSfile = os.path.join(inputs_path, 'all_models_{0}/ensemble_{1}_AMSfromHourly.csv'.format(Scen_dict[scenario], scenario))
    
    AMSdf = pd.read_csv(AMSfile, index_col=0)
    AMSmm = AMSdf.loc[:, '{}d'.format(duration)].values
    AMSinches = AMSmm / 25.4
    
    paras = distr.gev.lmom_fit(AMSinches)
    fitted_gev = distr.gev(**paras)

    precip = fitted_gev.ppf(AEP(ARI_list))
    return precip
        

# Generates a dataframe matching Atlas14 output table format, to be output as a CSV in sample_run
# columns = recurrence intervals, rows = durations
def CSVoutput(inputs_path, loc='MSP', fromWRF=True, fromHourly=True, scenario='HIST', model='', fullrecord=True, init=1837, end=2010,
              multiScenario=False, scenario2='MID'):
    
    df = pd.DataFrame(data=None, index=dur_list, columns=ARI_list)
    if fromHourly:
        for dur in dur_list:
            df.loc[dur] = PrcpAmts_fromHourly(inputs_path, scenario, dur)
    else:
        for dur in dur_list:
            df.loc[dur] = PrcpAmts(inputs_path, loc, dur, fromWRF, scenario, model, fullrecord, init, end, multiScenario, scenario2)

    return df

