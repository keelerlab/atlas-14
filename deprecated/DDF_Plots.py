'''
This code calculates a new csv from the csv output of a14lm
(depth-duration frequencies) with a cubic spline smoothing function.
The 8 models' values can be averaged either before or after smoothing.

CSVoutputs have to be created before plotting -
previous plot function performed smoothing independently.
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import interpolate, stats
import os
from AMS_to_DDF import GEVfit
import probscale

durmin=1
durmax=60
dur_list = [1,2,3,4,7,10,20,30,45,60]

loc_dict = {'ALX':'Alexandria', 'DLH':'Duluth', 'MSP':'Minneapolis', \
            'RST':'Rochester', 'WRN':'Warren' }
scenario_list = ['HIST', 'MID', 'END4.5', 'END8.5']
model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}
    

class Input:
    def __init__(self, dirc, loc, scenario, smoothed):
        model = 'all'
        # could restore "model" as an argument if any reason to look at individual ones
        
        if smoothed:
            CSVinput = os.path.join(dirc,scenario,loc,'{}_DDF_smoothed.csv'.format(model))
        else:
            CSVinput = os.path.join(dirc,scenario,loc,'{}_DDF.csv'.format(model))
        
        #DDF: depth-duration frequency
            #not to be confused with DF as abbrev for DataFrame
        self.DDF = pd.read_csv(CSVinput, index_col=0)

        rows = list(self.DDF.index.values)
        self.durs = rows[ rows.index(durmin) : rows.index(durmax)+1 ]



def plot_byRecurrence(in_path, fig_outpath, loc='Douglas', scenario='HIST', smoothed=False):  
    
    DDF = Input(in_path, loc, scenario, smoothed).DDF
    durs = dur_list
    
    for AEP in DDF.columns:
        depths = DDF.loc[durmin:durmax, str(AEP)].values
        plt.semilogx(durs, depths)
    
    plt.title('{0} County ({1})'.format(loc, scenario))
    plt.xlabel('Duration (days)')
    plt.ylabel('Precipitation depth (in)')
    legendstr = 'Annual exceedance \nprobability (1/years)'
    plt.legend(DDF.columns.values, title=legendstr, loc=0, bbox_to_anchor=(1,0.9))
    plt.xticks(ticks=durs, labels=durs)
    plt.xlim(durmin, durmax)
    #plt.ylim(-1,29)
    plt.grid(linestyle='--')
    
    if smoothed:
        smoothed_pathstr = 'smoothed_'
    else:
        smoothed_pathstr = ''    
    fig_outpath_full = os.path.join(fig_outpath,
                                    '{0}_{1}_{2}byRecurrence.png'.format(loc, scenario, smoothed_pathstr))  
    plt.savefig(fig_outpath_full, dpi=150, bbox_inches='tight')
    plt.close()
    
def plot_byDuration(in_path, fig_outpath, loc='Douglas', scenario='HIST', smoothed=False):  
    
    DDF = Input(in_path, loc, scenario, smoothed).DDF
    DDF.columns = DDF.columns.astype('int')
    AEPs = list(DDF.columns)
    
    for dur in DDF.index:
        depths = DDF.loc[dur, :].values
        plt.semilogx(AEPs, depths)
    
    plt.title('{0} County ({1})'.format(loc, scenario))

    
    plt.xlabel('Annual exceedance probability (1/years)')
    plt.ylabel('Precipitation depth (in)')
    legendstr = 'Duration (days)'
    plt.legend(DDF.index.values, title=legendstr, loc=0, bbox_to_anchor=(1,0.9))
    plt.xticks(ticks=AEPs, labels=AEPs)
    plt.xlim(min(DDF.columns), max(DDF.columns))
    #plt.ylim(-1,29)
    plt.grid(linestyle='--')
    
    if smoothed:
        smoothed_pathstr = 'smoothed_'
    else:
        smoothed_pathstr = ''    
    fig_outpath_full = os.path.join(fig_outpath,
                                    '{0}_{1}_{2}byDuration.png'.format(loc, scenario, smoothed_pathstr))  
    plt.savefig(fig_outpath_full, dpi=150, bbox_inches='tight')
    plt.close()

    
        