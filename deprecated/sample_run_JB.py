import a14_lm
from pathlib import Path
import os

scenario_list = ['HIST', 'MID', 'END4.5', 'END8.5']
loc_list = ['MSP', 'DLH', 'ALX', 'RST', 'WRN']
model_list = ['BC', 'CC', 'CM', 'CN', 'GF', 'IP', 'MR', 'MI']

inputs_path_obsv = '/Users/birke111/Documents/Atlas14/inputs/'
# inputs_path_wrf = '/Users/birke111/Documents/Climate Data/derived_stats'
inputs_path_wrf = 'county_AMS/'
inputs_path_fromHourly = '/Users/birke111/Documents/Hourly_CSV'
# base_outpath = '/Users/birke111/Documents/Atlas14_test/'
base_outpath = '/test/'


fromWRF = True         # set to False if using observational data instead
modelAvgAMS = True     # average each year's maxima across 8 models - otherwise each model calculated individually

# set these 2 to False if fromWRF=False
fromHourly = False
multiScenario = False

# applicable if from observational record (fromWRF=False)
fullRecord = True # otherwise Init and End denote which subset of record to use (1980-1999 matches HIST model scenario)
Init = 1980         
End = 1999
#   Note: Warren (WRN) record ends in 1975, so will produce an error.
#   Selected subset needs >3yr overlap with actual years available for lmoments to work.

'''
This is the primary set of loops needed to produce DDF tables from either observational or model (fromWRF) data.
From either data set, daily precip sums are used from one location at a time.
Other loops (below) calculate from hourly precip (ensemble averages of each hour) 
    or pair two model scenarios together into a virtual 40-year record.
'''
def makeDDF_CSVs(Model):
    for Loc in loc_list:        
        if fromWRF:
            for Scenario in scenario_list:
                df = a14_lm.CSVoutput(inputs_path_wrf, loc=Loc, fromWRF=True, scenario=Scenario, model=Model)
                WRF_outpath = os.path.join(base_outpath, 'AMS_WRF outputs', Scenario, Loc)
                Path(WRF_outpath).mkdir(parents=True, exist_ok=True)  # Creates missing directories
                df.to_csv(os.path.join(WRF_outpath, '{}_DDF.csv'.format(Model)))
        else:
            df = a14_lm.CSVoutput(inputs_path_obsv, loc=Loc, fromWRF=False, fullrecord=fullRecord, init=Init, end=End)
            obsv_outpath = os.path.join(base_outpath, 'AMS_obsv outputs/{}'.format(Loc)) 
            Path(obsv_outpath).mkdir(parents=True, exist_ok=True)
            if fullRecord:
                filename = 'fullrecord DDF.csv'
            else:
                filename = '{0}-{1} DDF.csv'.format(Init, End)
            df.to_csv(os.path.join(obsv_outpath, filename))


if fromHourly:      # currently have hourly model outputs for the MSP gridcell only
    for Scenario in scenario_list:
        df = a14_lm.CSVoutput(inputs_path_fromHourly, fromHourly=True, scenario=Scenario)
        fromHourly_outpath = os.path.join(base_outpath, 'AMS_fromHourly outputs')
        Path(fromHourly_outpath).mkdir(parents=True, exist_ok=True)
        df.to_csv(os.path.join(fromHourly_outpath, '{}_DDF.csv'.format(Scenario)))

elif multiScenario:
    scenarios = [['HIST','MID'],['MID','END4.5'],['MID','END8.5']]
    Model = 'all'
    for Loc in loc_list:
        for a in range(len(scenarios)):
            df = a14_lm.CSVoutput(inputs_path_wrf, loc=Loc, fromWRF=True, scenario=scenarios[a][0], model=Model, multiScenario=True, scenario2=scenarios[a][1])
            Scenario = scenarios[a][0] + '+' + scenarios[a][1]
            WRF_outpath = os.path.join(base_outpath, 'AMS_WRF outputs', Scenario, Loc)
            Path(WRF_outpath).mkdir(parents=True, exist_ok=True)
            df.to_csv(os.path.join(WRF_outpath, '{}_DDF.csv'.format(Model)))

else:
    if fromWRF:
        if modelAvgAMS:
            makeDDF_CSVs(Model='all')
        else:
            for M in model_list:
                makeDDF_CSVs(Model=M)
    else:
        makeDDF_CSVs(Model='')
        


