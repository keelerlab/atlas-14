import AMS_to_DDF as DDF
from deprecated import DDF_Comparisons as DDFcomp
import DDF_Plots as Plots
from pathlib import Path
import os

scenario_list = ['HIST', 'MID', 'END4.5', 'END8.5']

loc_list = ['Aitkin', 'Anoka', 'Becker', 'Beltrami', 'Benton', 'Big Stone', 'Blue Earth', 'Brown', 'Carlton', 'Carver',
            'Cass', 'Chippewa', 'Chisago', 'Clay', 'Clearwater', 'Cook', 'Cottonwood', 'Crow Wing', 'Dakota', 'Dodge',
            'Douglas', 'Faribault', 'Fillmore', 'Freeborn', 'Goodhue', 'Grant', 'Hennepin', 'Houston', 'Hubbard',
            'Isanti', 'Itasca', 'Jackson', 'Kanabec', 'Kandiyohi', 'Kittson', 'Koochiching', 'Lac qui Parle', 'Lake',
            'Lake of the Woods', 'Le Sueur', 'Lincoln', 'Lyon', 'McLeod', 'Mahnomen', 'Marshall', 'Martin', 'Meeker',
            'Mille Lacs', 'Morrison', 'Mower', 'Murray', 'Nicollet', 'Nobles', 'Norman', 'Olmsted', 'Otter Tail',
            'Pennington', 'Pine', 'Pipestone', 'Polk', 'Pope', 'Ramsey', 'Red Lake', 'Redwood', 'Renville', 'Rice',
            'Rock', 'Roseau', 'St. Louis', 'Scott', 'Sherburne', 'Sibley', 'Stearns', 'Steele', 'Stevens', 'Swift',
            'Todd', 'Traverse', 'Wabasha', 'Wadena', 'Waseca', 'Washington', 'Watonwan', 'Wilkin', 'Winona', 'Wright',
            'Yellow Medicine']

model_list = ['BC', 'CC', 'CM', 'CN', 'GF', 'IP', 'MR', 'MI']
row_drop_list = [0, 1, 2, 3]

inputs_path_obsv = '/Users/birke111/Documents/Atlas14/inputs/'
# inputs_path_wrf = '/Users/birke111/Documents/Climate Data/county_ams'
# base_outpath = '/Users/birke111/Documents/Atlas14_test2/'
inputs_path_wrf = 'C:/Users/rrnoe/Work/Stormwater/v2/county_ams'

fromWRF = True
# Set to False if using observational data instead
# If grid-interpolated observations are used later, will need new set of paths etc.
# In the meantime: this should just be left as True. 


# Only applicable if using observations (fromWRF=False)
fullRecord = True  # otherwise Init and End (inclusive) denote which subset of record to use (1980-1999 matches HIST model scenario)
Init = 1980
End = 1999


#   Note: Warren (WRN) record ends in 1975, so will produce an error.
#   Selected subset needs >3yr overlap with actual years available for lmoments to work.


def makeDDF_CSVs(Model, smoothing=False, max100=True, drop_rows=0):
    for Loc in loc_list:
        if fromWRF:
            for Scenario in scenario_list:
                df = DDF.output_forCSV(inputs_path_wrf, loc=Loc, fromWRF=True,
                                       scenario=Scenario, model=Model,
                                       applySmoothing=smoothing, max_100yr=max100, n_row_drop=drop_rows)

                WRF_outpath = os.path.join(base_outpath, 'DDF_fromWRF outputs', Scenario, Loc)
                Path(WRF_outpath).mkdir(parents=True, exist_ok=True)  # Creates missing directories
                filename = '{}_DDF'.format(Model)
                if smoothing:
                    filename = filename + '_smoothed'
                df.to_csv(os.path.join(WRF_outpath, '{}.csv'.format(filename)))

        else:
            # (this option is currently irrelevant but could be applicable again later with grid obs)

            df = DDF.output_forCSV(inputs_path_obsv, loc=Loc, fromWRF=False,
                                   fullrecord=fullRecord, init=Init, end=End,
                                   applySmoothing=smoothing, max_100yr=max100, n_row_drop=drop_rows)

            obsv_outpath = os.path.join(base_outpath, 'AMS_obsv outputs', Loc)
            Path(obsv_outpath).mkdir(parents=True, exist_ok=True)
            if fullRecord:
                filename = 'fullrecord DDF'
            else:
                filename = '{0}-{1} DDF'.format(Init, End)
            if applySmoothing:
                filename = filename + '_smoothed'
            df.to_csv(os.path.join(obsv_outpath, '{}.csv'.format(filename)))


def plots_loop():
    for Loc in loc_list:
        for Scenario in scenario_list:
            for smoothing in [True, False]:
                DDF_inpath = os.path.join(base_outpath, 'DDF_fromWRF outputs')
                fig_outpath = os.path.join(base_outpath, 'plots', Scenario, Loc)
                Path(fig_outpath).mkdir(parents=True, exist_ok=True)

                Plots.plot_byRecurrence(DDF_inpath, fig_outpath, Loc, Scenario, smoothing)
                Plots.plot_byDuration(DDF_inpath, fig_outpath, Loc, Scenario, smoothing)


# This generates both CSVs and heatmaps (same %change values in 2 formats).
# Currently set up to compare HIST to future scenarios, both with smoothed values.
# Could also feasibly compare smoothed and unsmoothed values from same scenario - 
#   Comp.compare just takes 2 file paths as inputs
#   from there, Comp.heatmap could take 'HIST' and 'HISTsmoothed' as scenario strings, etc.

def comparison_loop():
    for Loc in loc_list:
        for Scenario in ['MID', 'END4.5', 'END8.5']:
            DDF_inpath = os.path.join(base_outpath, 'DDF_fromWRF outputs')

            hist_DDF = os.path.join(DDF_inpath, 'HIST', Loc, 'all_DDF_smoothed.csv')
            future_DDF = os.path.join(DDF_inpath, Scenario, Loc, 'all_DDF_smoothed.csv')

            comp_outpath = os.path.join(base_outpath, 'comparisons', Loc)
            Path(comp_outpath).mkdir(parents=True, exist_ok=True)

            df = DDFcomp.compare(hist_DDF, future_DDF)
            df.to_csv(os.path.join(comp_outpath, 'HIST_to_{}.csv'.format(Scenario)))

            DDFcomp.heatmap(hist_DDF, future_DDF, comp_outpath, Loc, 'HIST', Scenario)


# for applySmoothing in [True, False]:
for n in row_drop_list:
    base_outpath = f'c:/users/rrnoe/Work/Stormwater/v2/countyDDF_new_aep/county{20 - n}yr/'
    for applySmoothing in [True]:
        if fromWRF:
            makeDDF_CSVs('all', applySmoothing, True, drop_rows=n)
        else:
            makeDDF_CSVs('', applySmoothing, True)

# plots_loop()
# comparison_loop()
