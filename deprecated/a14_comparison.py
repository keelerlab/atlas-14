'''
Compares tabular outputs between model scenarios, or HIST and observed (official Atlas-14
values or non-regiionalized, single-station-based values calculated here).
Comparisons expressed as percentages.
    
Heatmap function gives a visual representation of this comparison on a 2d basis.

'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
import os
from pathlib import Path


loc_dict = {'ALX':'Alexandria', 'DLH':'Duluth', 'MSP':'Minneapolis', \
            'RST':'Rochester', 'WRN':'Warren' }
scenario_dict = {'HIST', 'MID', 'END4.5', 'END8.5'}
model_dict = {'BC':'bcc-csm1-1', 'CC':'CCSM4', 'CM':'CMCC-CM', \
              'CN':'CNRM-CM5', 'GF':'GFDL-ESM2M', 'IP':'IPSL-CM5A-LR', \
              'MR':'MRI-CGCM3', 'MI':'MIROC5'}

class Input:
    def __init__(self, loc, model, scenario):
        #scenario can be 'obsv' or 'NOAA' along with the usual
     
        dirc_obsv = '/Users/birke111/Documents/Atlas14/{} outputs'.format(loc)
        dirc_wrf = '/Users/birke111/Documents/Atlas14/AMS_WRF outputs/{0}/{1}'.format(scenario, loc)
        
        NOAAfile = os.path.join(dirc_obsv, 'Annual Maximum Series/All_Depth_English_AMS.csv')
        rowrange = [13,23,24,25,26,27,28,29,30,31,32]
        self.NOAA_df = pd.read_csv(NOAAfile, usecols=range(0,10), index_col=0, \
                                  skiprows=lambda x: x not in rowrange) 
        
        if scenario == 'NOAA':
            self.df = self.NOAA_df
        elif scenario == 'obsv':
            DDF = os.path.join(dirc_obsv, 'AMS-based DDF.csv')
            self.df = pd.read_csv(DDF, index_col=0)
        else:
            DDF = os.path.join(dirc_wrf, '{}_DDF.csv'.format(model))
            self.df = pd.read_csv(DDF, index_col=0)

              
        #DDFsmoothed = os.path.join(dirc_wrf, '{}_DDF smoothed.csv'.format(model))
        #self.dfSmoothed = pd.read_csv(DDFsmoothed, index_col=0)
 
'''
for now: either evaluate recalculation of actual atlas14 (scenario1='NOAA', scenario2='obsv')
                or compare 2 model scenarios (sce
later options?
        compare HIST model to actual atlas14 (OR my recalculation of it)
        compare different models in same scenario? (maybe not as useful?)
        
'''        
def compare(loc='MSP', model='all', scenario1='HIST', scenario2='MID', smoothed=False):
    Scen1 = Input(loc, model, scenario1)
    NOAAdf = Scen1.NOAA_df
    
    #if smoothed: (use smoothed Input obj, ignoring this option for now)

    #if scenario1 == 'NOAA':
    #    old = NOAAdf
    #else:
    old = Scen1.df
    new = Input(loc, model, scenario2).df
    
    New = new.copy(deep=True)
    old.columns, New.columns = NOAAdf.columns, NOAAdf.columns
    old.index, New.index = NOAAdf.index, NOAAdf.index
    
    percent_chg = (New/old)*100
    return percent_chg

def improvement(loc='MSP'):
    #how much did smoothing improve values?    
    spline_corr = abs(compare(loc, False)-100) - abs(compare(loc, True)-100)
    
    return spline_corr

def comparison_csv(loc='MSP', model='all', scenario1='HIST', scenario2='MID', smoothed=False):
    outpath = '/Users/birke111/Documents/Atlas14/scenario comparisons/{}'.format(loc)
    Path(outpath).mkdir(parents=True, exist_ok=True)
    filename = '{0} to {1}.csv'.format(scenario1, scenario2)
    
    df = compare(loc, model, scenario1, scenario2, smoothed)
    df.to_csv(os.path.join(outpath, filename))
    
def csv_all(model='all', smoothed=False):
    scenario_pairs = [['obsv', 'HIST'], ['HIST', 'MID'], ['HIST', 'END4.5'], ['HIST', 'END8.5']]  
    
    for loc in loc_dict.keys():
        for [s1, s2] in scenario_pairs:
            comparison_csv(loc, model, s1, s2, smoothed)
        



#display = 'comp' or 'imp'    
def heatmap(display='comp', loc='MSP', model='all', scenario1='HIST', scenario2='MID', smoothed=False):
    if display == 'comp':
        df = compare(loc, model, scenario1, scenario2, smoothed)
        
        scen1_str = scenario1
        if scenario1 == 'obsv':
            scen1_str = 'observational'
        #plt.title('DDF comparison to Atlas 14 values ({})'.format(loc_dict[loc]))
        plt.title('Change from {0} to {1} ({2})'.format(scen1_str, scenario2, loc_dict[loc]))
        sidebar_str = 'Relative precipitation depth (%)'
        #VMin = 85
        #VMax = 115
        VMin = 75
        VMax = 125
    if display == 'imp':
        df = improvement(loc)
        plt.title('Improvement from smoothing, {}'.format(loc_dict[loc]))
        sidebar_str = 'Percent difference'
        VMin = -10
        VMax = 10
    df = df.astype(int)
    sb.heatmap(df, vmin=VMin, vmax=VMax, cmap ='RdYlBu', linewidths = 0.30, 
               annot = True, fmt='d', cbar_kws={'label': sidebar_str})
    plt.xlabel('Annual exceedance probability')
    
    '''
    plt.imshow(df, cmap ="RdYlBu") 
    plt.colorbar()
    plt.xticks(range(len(df)), df.columns) 
    plt.yticks(range(len(df)), df.index) 
    plt.show()
    '''
    
def multiHeatmap(loc='MSP', model='all', smoothed=False):
    fig, axes = plt.subplots(2, 2, figsize=(13,9))
    scenario_pairs = [['obsv', 'HIST'], ['HIST', 'MID'], ['HIST', 'END4.5'], ['HIST', 'END8.5']]  
    
    fig.subplots_adjust(hspace=0.35, wspace=0.35)
    VMin = 75
    VMax = 125
    sidebar_str = 'Relative depth (%)'
    
    for ax, [scenario1, scenario2] in zip(axes.flatten(), scenario_pairs):
        df = compare(loc, model, scenario1, scenario2, smoothed)
        df = df.astype(int)
        
        scen1_str = scenario1
        if scenario1 == 'obsv':
            scen1_str = 'Observational'
        
        ax.set_title('{0} to {1}'.format(scen1_str, scenario2))
        ax.set_xlabel('Annual exceedance probability')
        
        sb.heatmap(ax=ax, data=df, vmin=VMin, vmax=VMax, cmap ='RdYlBu', linewidths = 0.30, 
               annot = True, fmt='d', cbar_kws={'label': sidebar_str})
    
    # modify this if any reason to just compare scenarios for one model?
    fig.suptitle('Relative scenario precipitation depths ({} ensemble average)'.format(loc_dict[loc]), fontsize=16)

    